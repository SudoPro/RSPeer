package com.sudo.rspeer.scripts.sudofisher.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import com.sudo.rspeer.scripts.sudofisher.enums.BankingType;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.DepositBox;

public class IsBankOpen extends BranchTask {
    private SudoFisher script;

    public IsBankOpen (SudoFisher script){
        this.script = script;
    }

    @Override
    public TreeTask successTask() {
        if (script.bankingType == BankingType.withdrawEquipment)
            return script.withdrawEquipmentLeaf;
        else
            return script.depositAllLeaf;
    }

    @Override
    public boolean validate() {
        validate = Bank.isOpen() || DepositBox.isOpen();
        Debug.log(script.player.getName(), "IsBankOpen", validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return script.isBankAround;
    }
}
