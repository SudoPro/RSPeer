package com.sudo.rspeer.scripts.sudofisher.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.game.hud.SudoInventory;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import com.sudo.rspeer.scripts.sudofisher.enums.BankingType;
import com.sudo.rspeer.scripts.sudofisher.enums.FishLocation;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;

public class IsInventoryFull extends BranchTask {
    private SudoFisher script;

    public IsInventoryFull(SudoFisher script){
        this.script = script;
    }

    @Override
    public TreeTask successTask() {
        if (script.isPowerFishing) {
            if(script.fishLocation == FishLocation.BAXTORIAN_FALLS){
                script.powerDropLeaf.setValues(SudoInventory.getItemNamesWithNameContains("Leaping"));
            } else {
                script.powerDropLeaf.setValues(SudoInventory.getItemNamesWithNameContains("Raw"));
            }
            return script.powerDropLeaf;
        } else {
            script.bankingType = BankingType.depositAllExceptEquipment;
            return script.isBankOpen;
        }
    }

    @Override
    public boolean validate() {
        if(script.powerFishInventoryCheck <= 0)
            script.powerFishInventoryCheck = Random.nextInt(10, 27);

        validate =  script.isPowerFishing ? Inventory.getItems().length > script.powerFishInventoryCheck : Inventory.isFull();
        Debug.log(script.player.getName(), "IsInventoryFull", validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return script.isFishingSpotAround;
    }
}
