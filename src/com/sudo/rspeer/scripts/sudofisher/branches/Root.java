package com.sudo.rspeer.scripts.sudofisher.branches;

import com.sudo.rspeer.base.SudoRootTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.game.hud.SudoInventory;
import com.sudo.rspeer.api.util.SudoMath;
import com.sudo.rspeer.common.leafs.PowerDropLeaf;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudofisher.enums.BankingType;
import com.sudo.rspeer.scripts.sudofisher.enums.FishLocation;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;

import java.text.NumberFormat;
import java.util.Locale;

public class Root extends SudoRootTask {
    private SudoFisher script;

    public Root(SudoFisher script) {
        super(script);
        this.script = script;
    }

    @Override
    public void updateDisplayMap() {
        script.displayInfoMap.put("Fish Count: ", Integer.toString(script.fishCount) +
                " (" + NumberFormat.getNumberInstance(Locale.US).format(SudoMath.getItemPerHour(script.stopwatch, script.fishCount)) + "/Hr)");
    }

    @Override
    public TreeTask rootTask() {
        if (script.firstLogin) {
            if (Game.isLoggedIn()) {
                // Add Skills to track to the HashMap
                //script.XPInfoMap.put(Skill.FISHING, new XPInfo(Skill.FISHING));

                script.firstLogin = false;

                Debug.log("Fishing Level: " + Skills.getCurrentLevel(Skill.FISHING));
                Debug.log("Location: " + script.locationNameString);
                Debug.log("Fish: " + script.fishNameString);
                Debug.log("Powerfishing: " + script.isPowerFishing);
                Debug.log("UseDepositBox: " + script.useDepositBox);
                Debug.log("UsingPreset: " + script.usingPreset);
                if(script.usingPreset)
                    Debug.log("Preset #: " + script.presetNumber);
                Debug.log("UsingDragonHarpoon: " + script.usingDragonHarpoon);

                script.powerDropLeaf = new PowerDropLeaf(script) {
                    @Override
                    public void successAction() {
                        script.powerFishInventoryCheck = 0;
                    }
                };
            }
        }

        if(script.usingDragonHarpoon && script.checkSpecialTimer != null && !script.checkSpecialTimer.hasStarted())
            script.checkSpecialTimer.start();

        validate = SudoInventory.getItemsWithNameContains("Raw").length > 0 || SudoInventory.getItemsWithNameContains("Leaping").length > 0;
        Debug.log(script.player.getName() + " -> Root:HasRaw: " + validate);
        Debug.log(script.player.getName() + " -> Root:CurrentlyDepositing: " + script.isDepositing);
        Debug.log(script.player.getName() + " -> Root:CurrentlyDropping: " + script.isDropping);
        if (validate) {
            if (script.isDepositing) {
                return script.useDepositBoxLeaf;
            } else if (script.isDropping){
                if(script.fishLocation == FishLocation.BAXTORIAN_FALLS){
                    script.powerDropLeaf.setValues(SudoInventory.getItemNamesWithNameContains("Leaping"));
                } else {
                    script.powerDropLeaf.setValues(SudoInventory.getItemNamesWithNameContains("Raw"));
                }
                return script.powerDropLeaf;
            }
        }else{
            if(script.isDepositing) {
                script.isDepositing = false;
            }
            if (script.isDropping){
                script.isDropping = false;
            }
        }
        validate = script.hasEquipment();
        Debug.log(script.player.getName() + " -> RootHasEquipment: " + validate);
        if (validate) {
            if(Skills.getCurrentLevel(Skill.ATTACK) >= 60 && Skills.getCurrentLevel(Skill.FISHING) >= 61) {
                if (script.fishAction.equals("Harpoon") && Inventory.contains("Dragon harpoon")) {
                    return script.wieldHarpoonLeaf;
                } else if (script.usingDragonHarpoon && script.checkSpecialTimer.hasExpired() && !script.bankArea.contains(script.player)) {
                    return script.useSpecialAttackLeaf;
                }
            }
            return script.isInventoryFull;
        } else {
            script.bankingType = BankingType.withdrawEquipment;
            return script.isBankOpen;
        }
    }


}
