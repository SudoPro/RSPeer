package com.sudo.rspeer.scripts.sudofisher.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import com.sudo.rspeer.scripts.sudofisher.enums.TraversalLocation;
import org.rspeer.runetek.api.scene.SceneObjects;


public class IsBankAround extends BranchTask {
    private SudoFisher script;

    public IsBankAround (SudoFisher script){
        this.script = script;
    }

    @Override
    public TreeTask successTask() {
        if (script.useDepositBox)
            return script.useDepositBoxLeaf;
        else {
            return script.openBankLeaf;
        }
    }

    @Override
    public boolean validate() {
        script.locatableBank = SceneObjects.getNearest(i ->
                i.getName().toLowerCase().contains(script.useDepositBox ? "deposit box" : "bank booth") && script.bankArea != null &&
                script.bankArea.contains(i.getPosition()));

        validate = script.locatableBank != null;
        Debug.log(script.player.getName(), "IsBankAround", validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        script.traversalLocation = TraversalLocation.bank;
        return script.traversalLeaf;
    }
}
