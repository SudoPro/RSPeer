package com.sudo.rspeer.scripts.sudofisher.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import com.sudo.rspeer.scripts.sudofisher.enums.TraversalLocation;

public class IsInArea extends BranchTask {
    private SudoFisher script;

    public IsInArea(SudoFisher script) {
        this.script = script;
    }

    @Override
    public TreeTask successTask() {
        if(script.traversalLocation == TraversalLocation.bank)
            return script.openBankLeaf;
        else if(script.traversalLocation == TraversalLocation.fishingArea)
            return script.isFishing;
        else
            return script.traversalLeaf;
    }

    @Override
    public boolean validate() {
        if (script.traversalLocation == TraversalLocation.bank) {
            validate = script.bankArea.contains(script.player);
            Debug.log(script.player.getName(), "IsInArea(bankArea)", validate);
            return validate;
        } else if (script.traversalLocation == TraversalLocation.fishingArea) {
            validate = Areas.anyContainsPositionable(script.fishAreas, script.player.getPosition());
            Debug.log(script.player.getName(), "IsInArea(fishArea)", validate);
            return validate;
        } else if (script.traversalLocation == TraversalLocation.fishingSpot) {
            // Always True
            Debug.log(script.player.getName(), "IsInArea(fishSpotArea)", true);
            return true;
        } else
            return false;
    }

    @Override
    public TreeTask failureTask() {
        return script.traversalLeaf;
    }
}