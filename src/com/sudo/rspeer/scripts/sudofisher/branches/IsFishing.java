package com.sudo.rspeer.scripts.sudofisher.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.game.entities.SudoPlayer;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;

public class IsFishing extends BranchTask {
    private SudoFisher script;

    public IsFishing(SudoFisher script){
        this.script = script;
    }

    @Override
    public TreeTask successTask() {
        return script.isFishingLeaf;
    }

    @Override
    public boolean validate() {
        validate = SudoPlayer.isTargetting("Fishing spot", "Rod Fishing spot");
        Debug.log(script.player.getName(), "IsFishing", validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return script.startFishingLeaf;
    }
}
