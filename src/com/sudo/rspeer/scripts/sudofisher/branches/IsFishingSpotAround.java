package com.sudo.rspeer.scripts.sudofisher.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import com.sudo.rspeer.scripts.sudofisher.enums.TraversalLocation;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.DepositBox;

public class IsFishingSpotAround extends BranchTask {
    private SudoFisher script;

    public IsFishingSpotAround(SudoFisher script) {
        this.script = script;
    }

    @Override
    public TreeTask successTask() {
        if (Bank.isOpen())
            return script.closeBankLeaf;
        else if (DepositBox.isOpen())
            return script.closeDepositBoxLeaf;
        else
            return script.isFishing;
    }

    @Override
    public boolean validate() {
        script.fishingSpot = script.findFishingSpot();
        validate = script.fishingSpot != null;
        Debug.log(script.player.getName(), "IsFishingSpotAround", validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        script.traversalLocation = TraversalLocation.fishingSpot;
        return script.isInArea;
    }
}