package com.sudo.rspeer.scripts.sudofisher.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import org.rspeer.runetek.api.commons.Time;

public class StartFishingLeaf extends LeafTask {
    private SudoFisher script;

    public StartFishingLeaf(SudoFisher script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Interacting with Fishing Spot.");
        if(script.fishingSpot.interact(script.fishAction))
            Time.sleepUntil(() -> script.player.distance(script.fishingSpot) < 3, 1000, 2000);
    }
}
