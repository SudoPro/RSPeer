package com.sudo.rspeer.scripts.sudofisher.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.game.hud.SudoDepositBox;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import org.rspeer.runetek.api.component.DepositBox;

public class UseDepositBoxLeaf extends LeafTask {
    private SudoFisher script;

    public UseDepositBoxLeaf (SudoFisher script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.isDepositing = true;

        if (DepositBox.isOpen()) {
            script.setCurrentTask("Depositing items");
            SudoDepositBox.depositAllExcept(script.allEquipment);
        } else {
            script.setCurrentTask("Opening Deposit Box");
            DepositBox.open();
        }
    }
}
