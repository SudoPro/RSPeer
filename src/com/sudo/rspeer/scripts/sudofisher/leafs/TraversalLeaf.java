package com.sudo.rspeer.scripts.sudofisher.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import com.sudo.rspeer.scripts.sudofisher.enums.TraversalLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public class TraversalLeaf extends LeafTask {
    private SudoFisher script;

    public TraversalLeaf(SudoFisher script){
        this.script = script;
    }

    @Override
    public void execute() {
        if(script.traversalLocation == TraversalLocation.bank)
        {
            script.setCurrentTask("Traveling to bank");
            Movement.walkTo(Areas.getRandomPosition(script.bankArea));
        }
        else if(script.traversalLocation == TraversalLocation.fishingArea)
        {
            script.setCurrentTask("Traveling to fishing area");
            Movement.walkTo(Areas.getRandomPosition(script.fishAreas.get(0)));
        }
        else if(script.traversalLocation == TraversalLocation.fishingSpot)
        {
            if(script.seenFishSpots.size() > 0) {
                script.setCurrentTask("Traveling to previously seen fishing spot");
                Position randomFishSpotPosition = script.seenFishSpots.get(Random.nextInt(0, script.seenFishSpots.size() - 1));
                Movement.walkTo(Areas.getRandomPosition(Area.rectangular(randomFishSpotPosition.getX() - 2, randomFishSpotPosition.getY() - 2, randomFishSpotPosition.getX() + 2, randomFishSpotPosition.getY() + 2)));
            }
            else {
                script.setCurrentTask("Attempting to find new fishing spot");
                Movement.walkTo(Areas.getRandomPosition(script.fishAreas.get(0)));
            }
        }

        // Delay after each attempt to traverse
        Time.sleep(500);
        Time.sleepUntil(() -> !script.player.isMoving(), 500, 2500);
    }
}
