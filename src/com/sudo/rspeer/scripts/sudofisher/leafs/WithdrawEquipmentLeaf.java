package com.sudo.rspeer.scripts.sudofisher.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import org.rspeer.runetek.api.component.Bank;

import java.util.Arrays;

public class WithdrawEquipmentLeaf extends LeafTask {
    private SudoFisher script;

    public WithdrawEquipmentLeaf(SudoFisher script) {
        this.script = script;
    }

    @Override
    public void execute() {
        String[] missingEquipment = script.getMissingEquipment().toArray(new String[0]);
        String missingAccessory = script.getMissingAccessory();
        if (missingEquipment.length > 0) {
            if (Bank.contains(missingEquipment)) {
                for (int i = 0; i < missingEquipment.length; i++) {
                    Debug.log(missingEquipment[i]);
                    if (Bank.contains(missingEquipment[i])) {
                        script.setCurrentTask("Withdrawing the equipment " + missingEquipment[i]);
                        Bank.withdraw(missingEquipment[i], 1);

                        if (missingEquipment[i].equals("Dragon harpoon"))
                            script.usingDragonHarpoon = true;
                        break;
                    }
                }
            } else {
                script.setCurrentTask("Bot stopping, couldn't find any of " + Arrays.toString(missingEquipment));
                script.setStopping(true);
            }
        } else if (!missingAccessory.equals("")) {
            if (missingAccessory.equals("Barbarian")) {
                if(Bank.contains("Feather")){
                    withdrawItem("Feather");
                } else if (Bank.contains("Fishing bait"))
                    withdrawItem("Fishing bait");
                else {
                    script.setCurrentTask("Bot stopping, couldn't find \"Feathers\" or \"Bait\".");
                    script.setStopping(true);
                }
            } else if (Bank.contains(missingAccessory)) {
                withdrawItem(missingAccessory);
            } else {
                script.setCurrentTask("Bot stopping, couldn't find \"" + missingAccessory + "\".");
                script.setStopping(true);
            }
        }
    }

    private void withdrawItem(String name){
        script.setCurrentTask("Withdrawing the equipment accessory \"" + name + "\".");
        Bank.withdraw(name, 100 + (((int) (Math.random() * 18)) * 50));
    }
}