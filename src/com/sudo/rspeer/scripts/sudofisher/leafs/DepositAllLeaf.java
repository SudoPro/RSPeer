package com.sudo.rspeer.scripts.sudofisher.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import org.rspeer.runetek.api.component.Bank;

public class DepositAllLeaf extends LeafTask {
    private SudoFisher script;

    public DepositAllLeaf (SudoFisher script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Depositing items...");
        Bank.depositAllExcept(script.allEquipment);
    }
}
