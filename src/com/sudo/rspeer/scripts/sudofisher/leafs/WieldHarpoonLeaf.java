package com.sudo.rspeer.scripts.sudofisher.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;

public class WieldHarpoonLeaf extends LeafTask {
    private SudoFisher script;

    public WieldHarpoonLeaf (SudoFisher script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.usingDragonHarpoon = true;
        if(Bank.isOpen()) {
            script.setCurrentTask("Closing bank to equip Dragon harpoon");
            Bank.close();
        }
        else{
            script.setCurrentTask("Attempting to equip Dragon harpoon");
            Item harpoon = Inventory.getFirst("Dragon harpoon");
            if (harpoon != null)
                harpoon.interact("Wield");
        }
    }
}
