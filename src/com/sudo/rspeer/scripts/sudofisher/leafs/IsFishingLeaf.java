package com.sudo.rspeer.scripts.sudofisher.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import org.rspeer.runetek.api.commons.Time;

public class IsFishingLeaf extends LeafTask {
    private SudoFisher script;

    public IsFishingLeaf(SudoFisher script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Currently fishing...");
        Time.sleep(250, 750);
    }
}
