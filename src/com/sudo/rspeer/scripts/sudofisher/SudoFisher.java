package com.sudo.rspeer.scripts.sudofisher;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.api.skills.model.XPInfo;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudofisher.branches.*;
import com.sudo.rspeer.scripts.sudofisher.enums.BankingType;
import com.sudo.rspeer.scripts.sudofisher.enums.FishLocation;
import com.sudo.rspeer.scripts.sudofisher.enums.TraversalLocation;
import com.sudo.rspeer.scripts.sudofisher.leafs.*;
import com.sudo.rspeer.scripts.sudofisher.ui.FisherFXController;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import org.rspeer.networking.api.RsPeerApi;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.component.tab.*;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.event.types.ItemTableEvent;
import org.rspeer.runetek.providers.RSItemDefinition;
import org.rspeer.script.ScriptMeta;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@ScriptMeta(developer = "Proxi", desc = "Intelligent fishing. Supports several locations and fish, equipment retrieval, presets, and more!", name = "SudoFisher")
public class SudoFisher extends SudoScript {

    public Area bankArea;
    public List<Area> fishAreas = new ArrayList<>();
    public List<Area> fishSpotAreas = new ArrayList<>();

    public int fishCount = 0, presetNumber = 0, powerFishInventoryCheck = 0, possibleFishCountFromLocation = 1;
    public String fishAction = "", fishSpotAction = "", fishNameString = "", locationNameString = "";

    public List<String> equipment = new ArrayList<>();
    public List<String> equipmentAccessory = new ArrayList<>();
    public String[] allEquipment;

    public ArrayList<Position> seenFishSpots = new ArrayList<>();
    public Npc fishingSpot;

    public TraversalLocation traversalLocation = TraversalLocation.bank;
    public BankingType bankingType = BankingType.depositAllExceptEquipment;
    public FishLocation fishLocation = FishLocation.GENERIC;

    public boolean useDepositBox = false, disableEquipmentCheck = false, isDepositing = false, usingPreset = false, usingDragonHarpoon = false, isPowerFishing = false;

    public SceneObject locatableBank;

    // Branches
    public Root root = new Root(this);
    public IsBankOpen isBankOpen = new IsBankOpen(this);
    public IsInventoryFull isInventoryFull = new IsInventoryFull(this);
    public IsBankAround isBankAround = new IsBankAround(this);
    public IsFishingSpotAround isFishingSpotAround = new IsFishingSpotAround(this);
    public IsFishing isFishing = new IsFishing(this);
    public IsInArea isInArea = new IsInArea(this);

    // Leafs
    public UseDepositBoxLeaf useDepositBoxLeaf = new UseDepositBoxLeaf(this);
    public WieldHarpoonLeaf wieldHarpoonLeaf = new WieldHarpoonLeaf(this);
    public WithdrawEquipmentLeaf withdrawEquipmentLeaf = new WithdrawEquipmentLeaf(this);
    public DepositAllLeaf depositAllLeaf = new DepositAllLeaf(this);
    public StartFishingLeaf startFishingLeaf = new StartFishingLeaf(this);
    public IsFishingLeaf isFishingLeaf = new IsFishingLeaf(this);
    public TraversalLeaf traversalLeaf = new TraversalLeaf(this);

    @Override
    public String getTitle() {
        return "SudoFisher v1.0.0";
    }

    @Override
    public String getTitleHexColor() {
        return "#00bbff";
    }

    @Override
    public BranchTask getRootTask() {
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();

        RsPeerApi.getCurrentUserName();

        // Add Skills to track to the HashMap
        xpInfoMap.put(Skill.FISHING, new XPInfo(Skill.FISHING));

        Platform.runLater(() -> {
            try {
                Debug.log("Trying to load fxml");
                URL fxmlURL = new URL("https://s3.amazonaws.com/sudobot/fxml/SudoFisher/FisherGUI+v1.0.0.fxml");
                //Debug.log("FXML URL: " + fxmlURL.toString());

                FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
                fxmlLoader.setController(new FisherFXController(this));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                scene.getStylesheets().add("https://s3.amazonaws.com/sudobot/fxml/css/modena_dark.css");
                jfxPanel.setScene(scene);

                SwingUtilities.invokeLater(() -> {
                    jFrame.add(jfxPanel);
                    jFrame.pack();
                    jFrame.setLocationRelativeTo(null);
                    jFrame.setVisible(true);
                }); // go for it
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public Npc findFishingSpot() {
        return findFishingSpot("Fishing spot", "Rod Fishing spot");
    }

    public Npc findFishingSpot(String... names) {
        Npc npc = null;
        Area currentArea = Areas.getAreaContaining(fishSpotAreas, player);

        Debug.log("FishAction: " + fishAction);
        Debug.log("FishSpotAction: " + fishSpotAction);

        if(currentArea != null)
            npc = Npcs.getNearest(n -> currentArea.contains(n.getPosition()) &&
                Arrays.asList(names).contains(n.getName()) &&
                Arrays.asList(n.getActions()).contains(fishAction) &&
                Arrays.asList(n.getActions()).contains(fishSpotAction));

        if(npc == null) {
            for (int i = 0; i < fishSpotAreas.size(); i++) {
                Area loopArea = fishSpotAreas.get(i);
                npc = Npcs.getNearest(n -> loopArea.contains(n.getPosition()) &&
                        Arrays.asList(names).contains(n.getName()) &&
                        Arrays.asList(n.getActions()).contains(fishAction) &&
                        Arrays.asList(n.getActions()).contains(fishSpotAction));

                if (npc != null)
                    break;
            }
        }

        return npc;
    }

    private boolean wieldingHarpoon() {
        EquipmentSlot equipmentSlot;
        return (equipmentSlot = EquipmentSlot.MAINHAND) != null &&
                (equipmentSlot.getItemName().equals("Dragon harpoon") || equipmentSlot.getItemName().equals("Barb-tail harpoon"));
    }

    public boolean hasEquipment() {
        if (disableEquipmentCheck)
            return true;

        if (fishAction.equals("Harpoon")) {
            if (Skills.getCurrentLevel(Skill.FISHING) >= 61) {
                EquipmentSlot equipmentSlot;
                if (Inventory.contains("Dragon harpoon") || (equipmentSlot = EquipmentSlot.MAINHAND) != null && equipmentSlot.getItemName().equals("Dragon harpoon")) {
                    usingDragonHarpoon = true;
                    return true;
                }
            }
            return Inventory.contains("Harpoon", "Barb-tail harpoon") || wieldingHarpoon();
        } else if (fishAction.equals("Bait"))
            return Inventory.contains("Fishing rod", "Barbarian rod") && Inventory.contains("Fishing bait");
        else if (equipmentAccessory.contains("Barbarian"))
            return Inventory.containsAll(equipment.toArray(new String[0])) && Inventory.contains("Feather", "Fishing bait");
        else
            return allEquipment != null && Inventory.containsAll(allEquipment);
    }

    public List<String> getMissingEquipment() {
        List<String> returnList = new ArrayList<>();

        if (fishAction.equals("Harpoon")) {
            if (Skills.getCurrentLevel(Skill.FISHING) >= 61) {
                if (!Inventory.contains("Harpoon", "Barb-tail harpoon", "Dragon harpoon") || wieldingHarpoon()) {
                    returnList.add("Dragon harpoon");
                    returnList.add("Barb-tail harpoon");
                    returnList.add("Harpoon");
                }
            } else {
                if (!Inventory.contains("Harpoon", "Barb-tail harpoon") || wieldingHarpoon()) {
                    returnList.add("Barb-tail harpoon");
                    returnList.add("Harpoon");
                }
            }
        } else if (fishAction.equals("Bait")) {
            if (!Inventory.contains("Fishing rod", "Barbarian rod")) {
                returnList.add("Barbarian rod");
                returnList.add("Fishing rod");
            }
        } else {
            for (String i : equipment) {
                if (!Inventory.contains(i))
                    returnList.add(i);
            }
        }

        return returnList;
    }

    public String getMissingAccessory() {

        if(equipmentAccessory.contains("Barbarian")){
            if(!Inventory.contains("Feather") || Inventory.contains("Fishing bait"))
                return "Barbarian";
        }
        for (String i : equipmentAccessory) {
            if (!Inventory.contains(i))
                return i;
        }

        return "";
    }

    @Override
    public void notify(ItemTableEvent event) {
        if(event != null) {
            if (event.getChangeType() == ItemTableEvent.ChangeType.ITEM_ADDED) {
                if(bankArea != null && player != null && !bankArea.contains(player)) {
                    super.onItemAdded(event);

                    RSItemDefinition definition = event.getDefinition();

                    if (definition != null && (definition.getName().contains("Raw") || definition.getName().contains("Leaping"))) {
                        fishCount++;
                    }
                }
            } else if( event.getChangeType() == ItemTableEvent.ChangeType.ITEM_REMOVED){
                if(!bankArea.contains(player))
                    super.onItemRemoved(event);
            }
        }
    }
}