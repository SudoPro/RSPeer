package com.sudo.rspeer.scripts.sudofisher.ui;

import com.sudo.rspeer.base.ui.fx.SudoFXController;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.scripts.sudofisher.SudoFisher;
import com.sudo.rspeer.scripts.sudofisher.enums.FishLocation;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the FisherFXGui class
 */
public class FisherFXController extends SudoFXController implements Initializable {

    private SudoFisher script;

    @FXML
    private ListView<String> locationsListView, fishListView;

    @FXML
    private Button startButton;

    @FXML
    private CheckBox disableEquipmentCheck_CB, powerFish_CB, useDepositBox_CB;

    @FXML
    private RadioButton one_Radio, two_Radio;

    public FisherFXController(SudoFisher script){
        super(script);
        this.script = script;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        // Add Locations to the ListView
        locationsListView.getItems().addAll("Draynor", "Lumbridge", "Barbarian Village", "Baxtorian Falls", "Al Kharid", "Catherby", "Piscatoris", "Fishing Guild", "Shilo Village (beta)");

        locationsListView.setOnMouseClicked(getLocationClickAction());
        fishListView.setOnMouseClicked(getfishListClickAction());

        useDepositBox_CB.setOnAction(useDepositBox_CBAction());
        powerFish_CB.setOnAction(powerFish_CBAction());

        startButton.setOnAction(getStartButtonAction());


    }

    private EventHandler<MouseEvent> getLocationClickAction() {
        return event -> {
            fishListView.getItems().clear();
            useDepositBox_CB.setDisable(false);
            startButton.setDisable(true);

            try {
                switch (locationsListView.getSelectionModel().getSelectedItem()) {
                    case "Draynor":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        break;

                    case "Lumbridge":
                        fishListView.getItems().add("Trout/Salmon");
                        fishListView.getItems().add("Pike");
                        fishListView.getItems().add("Rainbow");
                        useDepositBox_CB.setDisable(true);
                        powerFish_CB.setSelected(true);
                        break;

                    case "Al Kharid":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        break;

                    case "Barbarian Village":
                        fishListView.getItems().add("Trout/Salmon");
                        fishListView.getItems().add("Pike");
                        fishListView.getItems().add("Rainbow");
                        break;

                    case "Baxtorian Falls":
                        fishListView.getItems().add("Leaping trout/salmon/sturgeon");
                        break;

                    case "Catherby":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        fishListView.getItems().add("Mackerel/Cod/Bass");
                        fishListView.getItems().add("Lobster");
                        fishListView.getItems().add("Tuna/Swordfish");
                        fishListView.getItems().add("Shark");
                        break;

                    case "Piscatoris":
                        fishListView.getItems().add("Monkfish");
                        fishListView.getItems().add("Tuna/Swordfish");
                        break;

                    case "Fishing Guild":
                        fishListView.getItems().add("Mackerel/Cod/Bass");
                        fishListView.getItems().add("Lobster");
                        fishListView.getItems().add("Tuna/Swordfish");
                        fishListView.getItems().add("Shark");
                        break;

                    case "Bhurg De Rott":
                        fishListView.getItems().add("Sharks");
                        break;

                    case "Mobilising Armies":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        fishListView.getItems().add("Mackerel/Cod/Bass");
                        fishListView.getItems().add("Lobster");
                        fishListView.getItems().add("Tuna/Swordfish");
                        break;

                    case "Shilo Village (beta)":
                        fishListView.getItems().add("Trout/Salmon");
                        fishListView.getItems().add("Pike/Eels");
                        fishListView.getItems().add("Rainbow");
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    private EventHandler<MouseEvent> getfishListClickAction() {
        return event -> {
            startButton.setDisable(false);

            if (locationsListView.getSelectionModel().getSelectedItem().equals("Lumbridge"))
                useDepositBox_CB.setDisable(true);
            else
                useDepositBox_CB.setDisable(false);
        };
    }

    public EventHandler<ActionEvent> getStartButtonAction() {
        return event -> {
            super.getStart_BTEvent();
            String location = "", fish = "";

            script.fishLocation = FishLocation.GENERIC;

            try {
                location = locationsListView.getSelectionModel().getSelectedItem();
                fish = fishListView.getSelectionModel().getSelectedItem();
            } catch (Exception e) {
                System.out.println("Make sure you have both Fish and Location selected.");
            }

            if (location != null && fish != null) {
                try {
                    script.equipment = new ArrayList<>();
                    script.equipmentAccessory = new ArrayList<>();
                    switch (fish) {
                        case "Shrimp/Anchovies":
                            script.fishAction = "Small Net";
                            script.fishSpotAction = "Bait";
                            script.equipment.add("Small fishing net");
                            script.possibleFishCountFromLocation = 2;
                            break;
                        case "Sardines/Herring":
                            script.fishAction = "Bait";
                            script.fishSpotAction = "Bait";
                            script.equipment.add("Fishing rod");
                            script.equipmentAccessory.add("Fishing bait");
                            script.possibleFishCountFromLocation = 2;
                            break;
                        case "Trout/Salmon":
                            script.fishAction = "Lure";
                            script.fishSpotAction = "Lure";
                            script.equipment.add("Fly fishing rod");
                            script.equipmentAccessory.add("Feather");
                            script.possibleFishCountFromLocation = 2;
                            break;
                        case "Leaping trout/salmon/sturgeon":
                            script.fishAction = "Use-rod";
                            script.fishSpotAction = "Use-rod";
                            script.equipment.add("Barbarian rod");
                            script.equipmentAccessory.add("Barbarian");
                            script.possibleFishCountFromLocation = 3;
                            break;
                        case "Pike":
                            script.fishAction = "Bait";
                            script.fishSpotAction = "Lure";
                            script.equipment.add("Fishing rod");
                            script.equipmentAccessory.add("Fishing bait");
                            script.possibleFishCountFromLocation = 1;
                            break;
                        case "Pike/Eels":
                            script.fishAction = "Bait";
                            script.fishSpotAction = "Lure";
                            script.equipment.add("Fishing rod");
                            script.equipmentAccessory.add("Fishing bait");
                            script.possibleFishCountFromLocation = 2;
                            break;
                        case "Mackerel/Cod/Bass":
                            script.fishAction = "Big Net";
                            script.fishSpotAction = "Harpoon";
                            script.equipment.add("Big fishing net");
                            script.possibleFishCountFromLocation = 3;
                            break;
                        case "Rainbow":
                            script.fishAction = "Lure";
                            script.fishSpotAction = "Lure";
                            script.equipment.add("Fly fishing rod");
                            script.equipmentAccessory.add("Stripy feather");
                            script.possibleFishCountFromLocation = 1;
                            break;
                        case "Lobster":
                            script.fishAction = "Cage";
                            script.fishSpotAction = "Cage";
                            script.equipment.add("Lobster pot");
                            script.possibleFishCountFromLocation = 1;
                            break;
                        case "Monkfish":
                            script.fishAction = "Net";
                            script.fishSpotAction = "Harpoon";
                            script.equipment.add("Small fishing net");
                            script.possibleFishCountFromLocation = 1;
                            break;
                        case "Tuna/Swordfish":
                            switch (location) {
                                case "Piscatoris":
                                    script.fishAction = "Harpoon";
                                    script.fishSpotAction = "Net";
                                    script.equipment.add("Harpoon");
                                    break;

                                default:
                                    script.fishAction = "Harpoon";
                                    script.fishSpotAction = "Cage";
                                    script.equipment.add("Harpoon");
                                    break;

                            }
                            script.possibleFishCountFromLocation = 2;
                            break;
                        case "Shark":
                            if(location.equals("Catherby"))
                                script.fishSpotAction = "Big Net";
                            else
                                script.fishSpotAction = "Net";

                            script.fishAction = "Harpoon";

                            script.equipment.add("Harpoon");
                            script.possibleFishCountFromLocation = 1;
                            break;
                    }

                    switch (location) {
                        case "Lumbridge":
                            script.bankArea = Areas.LUMB_BANK_AREA;
                            script.fishAreas.add(Areas.LUMB_FISH_AREA);
                            script.fishSpotAreas.add(Areas.LUMB_FISH_SPOT_AREA);
                            break;
                        case "Draynor":
                            script.bankArea = Areas.DRAY_BANK_AREA;
                            script.fishAreas.add(Areas.DRAY_FISH_AREA);
                            script.fishSpotAreas.add(Areas.DRAY_FISH_AREA);
                            break;
                        case "Al Kharid":
                            script.bankArea = Areas.ALKHARID_OSRS_BANK_AREA;
                            script.fishAreas.add(Areas.ALKHARID_OSRS_FISH_AREA);
                            script.fishSpotAreas.add(Areas.ALKHARID_OSRS_FISH_AREA);
                            break;
                        case "Barbarian Village":
                            script.bankArea = Areas.EDGE_BANK_AREA;
                            script.fishAreas.add(Areas.BARB_FISH_AREA);
                            script.fishSpotAreas.add(Areas.BARB_FISH_AREA);
                            break;
                        case "Baxtorian Falls":
                            script.bankArea = Areas.BARB_ASSAULT_BANK_AREA;
                            script.fishAreas.add(Areas.BAXTORIAN_FISH_AREA);
                            script.fishSpotAreas.add(Areas.BAXTORIAN_FISH_AREA);
                            script.fishLocation = FishLocation.BAXTORIAN_FALLS;
                            break;
                        case "Catherby":
                            script.bankArea = Areas.CATH_OSRS_BANK_AREA;
                            script.fishAreas.add(Areas.CATH_FISH_AREA);
                            script.fishSpotAreas.add(Areas.CATH_FISH_AREA);
                            break;
                        case "Piscatoris":
                            script.bankArea = Areas.PISCATORIS_BANK_AREA;
                            script.fishAreas.add(Areas.PISCATORIS_FISH_AREA);
                            script.fishSpotAreas.add(Areas.PISCATORIS_FISH_AREA);
                            break;
                        case "Fishing Guild":
                            script.bankArea = Areas.GUILD_BANK_OSRS_AREA;
                            script.fishAreas.add(Areas.GUILD_DOCK1_FISH_AREA);
                            script.fishAreas.add(Areas.GUILD_DOCK2_FISH_AREA);
                            script.fishSpotAreas.add(Areas.GUILD_DOCK1_FISH_SPOT_AREA);
                            script.fishSpotAreas.add(Areas.GUILD_DOCK2_FISH_SPOT_AREA);
                            script.fishLocation = FishLocation.FISHGUILD;
                            break;
                        case "Mobilising Armies":
                            script.bankArea = Areas.ARMIES_BANK_AREA;
                            script.fishAreas.add(Areas.ARMIES_FISH_AREA);
                            script.fishSpotAreas.add(Areas.ARMIES_FISH_AREA);
                        case "Shilo Village (beta)":
                            script.bankArea = Areas.SHILO_BANK_AREA;
                            script.fishAreas.add(Areas.SHILO_FISH_AREA);
                            script.fishSpotAreas.add(Areas.SHILO_FISH_AREA);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                super.getStart_BTEvent();

                script.useDepositBox = useDepositBox_CB.isSelected();

                script.disableEquipmentCheck = disableEquipmentCheck_CB.isSelected();

                script. isPowerFishing = powerFish_CB.isSelected();

                script.locationNameString = location;
                script.fishNameString = fish;

                // Create temp arraylist and combine equipment and equipmentAccessory lists
                List<String> temp = new ArrayList<>();
                temp.addAll(script.equipment);
                temp.addAll(script.equipmentAccessory);

                // Create String array so it can later be used in BankInventoryTask
                // These items will not be banked
                script.allEquipment = temp.toArray(new String[temp.size()]);
                temp = null;

                startButton.textProperty().set("Update");
                script.waitingOnGUI = false;
            }
        };
    }

    private EventHandler<ActionEvent> useDepositBox_CBAction() {
        return event -> {

            powerFish_CB.setSelected(false);
        };
    }

    private EventHandler<ActionEvent> powerFish_CBAction() {
        return event -> {

            useDepositBox_CB.setSelected(false);
        };
    }
}