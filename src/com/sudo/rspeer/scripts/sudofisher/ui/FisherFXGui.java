package com.sudo.rspeer.scripts.sudofisher.ui;

import com.sudo.rspeer.api.util.Debug;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.concurrent.CountDownLatch;

public class FisherFXGui extends Application{


    public static final CountDownLatch latch = new CountDownLatch(10);
    public static FisherFXGui fisherFXGui = null;

    public FisherFXGui(){
        //Debug.log("GUI constructor called.");
        setFisherFXGui(this);
    }

    public static FisherFXGui waitingForStartup() {
        Debug.log("Attempting to latch.");
//        try {
//            latch.await();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return fisherFXGui;
    }

    public static void setFisherFXGui(FisherFXGui GUI){
        fisherFXGui = GUI;
        //Debug.log("Set GUI variable");
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader();

        //loader.setRoot(new GridPane());

        Debug.log("Attempting to load FXML..."); // this one
        Debug.log(getClass().toString());

        //InputStream stream = FisherFXGui.class.getResourceAsStream()

        // Load the FXML file ok
        GridPane root = loader.load(getClass().getResource("/com/sudo/rspeer/scripts/sudofisher/ui/fxml/FisherGUI.fxml"));
        //Parent root = loader.load(Paths.get("C:\\Users\\SudoPro\\IdeaProjects\\RSPeer\\src\\com\\sudo\\rspeer\\scripts\\sudofisher\\ui\\FisherGUI.fxml").toUri().toURL());

        stage.setTitle("SudoFisher Setup");
        Scene scene = new Scene(root, 600, 345);
        scene.getStylesheets().add("com/sudo/rspeer/base/ui/fx/css/SudoStyle.css");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

//        BorderPane pane = new BorderPane();
//        Scene scene = new Scene(pane, 500, 500);
//        stage.setScene(scene);
//
//        Label label = new Label("Hello");
//        pane.setCenter(label);
//
//        stage.show();
    }

    public static void main(String[] args) {
        Debug.log("In main, attempting to launch GUI...");
        launch(FisherFXGui.class, args);
    }
}
