package com.sudo.rspeer.scripts.sudofisher.enums;

public enum BankingType
{
    withdrawEquipment,
    depositAllExceptEquipment
}