package com.sudo.rspeer.scripts.sudofisher.enums;

public enum TraversalLocation
{
    bank,
    fishingArea,
    fishingSpot
}
