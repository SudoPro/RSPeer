package com.sudo.rspeer.scripts.sudofisher.enums;

public enum FishLocation {
    GENERIC,
    FISHGUILD,
    BAXTORIAN_FALLS
}