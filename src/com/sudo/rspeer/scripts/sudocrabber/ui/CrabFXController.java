package com.sudo.rspeer.scripts.sudocrabber.ui;

import com.sudo.rspeer.base.ui.fx.SudoFXController;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.areas.CrabAreas;
import com.sudo.rspeer.scripts.sudocrabber.areas.SpotAreas;
import com.sudo.rspeer.scripts.sudocrabber.enums.BotLocation;
import com.sudo.rspeer.scripts.sudocrabber.enums.Playstyle;
import com.sudo.rspeer.scripts.sudocrabber.enums.Potion;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import org.rspeer.runetek.api.movement.position.Position;

import java.net.URL;
import java.util.ResourceBundle;

public class CrabFXController extends SudoFXController implements Initializable {

    private final SudoCrabber script;

    @FXML
    private ComboBox playstyleComboBox, crabTypeComboBox, foodSelectionComboBox, potSelectionComboBox, foodQuantityComboBox, potionQuantityComboBox;

    @FXML
    private CheckBox worldHopCheckBox, bankCheckBox, outOfPotionsCheckBox, pickupThrownCheckBox, useSpecialCheckBox;

    @FXML
    private Slider playerTriggerSlider, playerHealSlider;

    @FXML
    private Text playerCountText, promptText, playerHealText;

    @FXML
    private TextField xTextField, yTextField, planeTextField;

    @FXML
    private Button startButton, setLocationButton;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        playstyleComboBox.getItems().addAll("Active", "Spot Hop", "Stand Still");
        crabTypeComboBox.getItems().addAll("Sand Crab (Zeah Shore)", "Sand Crab (Crab Island)", "Rock Crab (Fremennik)");
        crabTypeComboBox.getSelectionModel().select("Sand Crab (Zeah Shore)");


        foodSelectionComboBox.getItems().addAll("Select", "Shark", "Monkfish", "Swordfish", "Lobster", "Tuna", "Pineapple pizza");
        foodSelectionComboBox.getSelectionModel().select("Select");
        potSelectionComboBox.getItems().addAll("Select", "Sp Att (4)", "Sp Str (4)", "Sp Att & Str (4)", "Sp Combat (4)", "Ranged (4)", "Zamorak brew (4)", "Attack (4)", "Strength (4)", "Att & Str (4)");
        potSelectionComboBox.getSelectionModel().select("Select");

        // Handle player trigger value slider
        playerTriggerSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            playerCountText.setText(Integer.toString(newValue.intValue()));
            script.playerRunCount = newValue.intValue();
        });

        // Handle player heal value slider
        playerHealSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            playerHealText.setText(Integer.toString(newValue.intValue()) + "%");
            script.healPercent = newValue.doubleValue();
            script.userProvidedPercent = newValue.doubleValue();
        });

        foodSelectionComboBox.setOnAction(getFoodComboBoxEvent());
        potSelectionComboBox.setOnAction(getPotionComboBoxEvent());

        foodQuantityComboBox.setOnAction(getFoodQuantityEvent());
        potionQuantityComboBox.setOnAction(getPotionQuantityEvent());

        playstyleComboBox.setOnAction(getplaystyleComboBoxEvent());
        crabTypeComboBox.setOnAction(getcrabTypeComboBoxEvent());
        bankCheckBox.setOnAction(getbankCheckBoxEvent());
        startButton.setOnAction(getStartButtonAction());

        setLocationButton.setOnAction(getSetLocationButtonAction());
    }

    public CrabFXController(SudoCrabber script) {
        super(script);
        this.script = script;
    }

    private EventHandler<ActionEvent> getSetLocationButtonAction() {
        return event -> {
            promptText.setText("");
            System.out.println("Attempting to get player position.");
            if (script.player != null) {
                script.standStillCoord = script.player.getPosition();

                Platform.runLater(() -> {
                    xTextField.textProperty().set(Integer.toString(script.standStillCoord.getX()));
                    yTextField.textProperty().set(Integer.toString(script.standStillCoord.getY()));
                    planeTextField.textProperty().set(Integer.toString(script.standStillCoord.getFloorLevel()));
                    handleStartButton();
                });
            }
        };
    }

    public EventHandler<ActionEvent> getplaystyleComboBoxEvent() {
        return event ->
        {
            if (playstyleComboBox.getSelectionModel().getSelectedItem().toString().equals("Stand Still")) {
                xTextField.setDisable(false);
                yTextField.setDisable(false);
                planeTextField.setDisable(false);
                setLocationButton.setDisable(false);
            } else {
                promptText.setText(" ");
                xTextField.setDisable(true);
                yTextField.setDisable(true);
                planeTextField.setDisable(true);
                setLocationButton.setDisable(true);
            }

            worldHopCheckBox.setDisable(!playstyleComboBox.getSelectionModel().getSelectedItem().toString().equals("Stand Still"));
            playerTriggerSlider.setDisable(playstyleComboBox.getSelectionModel().getSelectedItem().toString().equals("Active"));

            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getcrabTypeComboBoxEvent() {
        return event -> {
            if (crabTypeComboBox.getSelectionModel().getSelectedItem().equals("Rock Crab (Fremennik)")) {
                playstyleComboBox.getItems().clear();
                playstyleComboBox.getItems().addAll("Active", "Stand Still");
                bankCheckBox.setDisable(true);
            } if(crabTypeComboBox.getSelectionModel().getSelectedItem().equals("Sand Crab (Crab Island)")){
                playstyleComboBox.getItems().clear();
                playstyleComboBox.getItems().addAll("Spot Hop", "Stand Still");
                bankCheckBox.setDisable(false);
            } else {
                playstyleComboBox.getItems().clear();
                playstyleComboBox.getItems().addAll("Active", "Spot Hop", "Stand Still");
                bankCheckBox.setDisable(false);
            }
            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getbankCheckBoxEvent() {
        return event -> {
            foodSelectionComboBox.setDisable(!bankCheckBox.isSelected());
            potSelectionComboBox.setDisable(!bankCheckBox.isSelected());

            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getFoodComboBoxEvent() {
        return event -> {
            String selectedFood = getFoodSelected();
            foodQuantityComboBox.setDisable(selectedFood.equals("Select"));

            foodQuantityComboBox.getItems().clear();

            if (!selectedFood.equals("Select")) {
                for (int i = 1; i < 28; i++) { // - potionQuantity; i++) {
                    foodQuantityComboBox.getItems().add(i);
                }
                foodQuantityComboBox.getSelectionModel().select("10");
            }

            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getPotionComboBoxEvent() {
        return event -> {
            String selectedPotion = getPotionSelected();
            potionQuantityComboBox.setDisable(selectedPotion.equals("Select"));

            potionQuantityComboBox.getItems().clear();

            if (!selectedPotion.equals("Select")) {
                if (selectedPotion.equals("Sp Att & Str (4)")) {
                    for (int i = 1; i < 15; i++) { // - (foodQuantity * 2); i++) {
                        potionQuantityComboBox.getItems().add(i + " each");
                    }
                    potionQuantityComboBox.getSelectionModel().select("5 each");
                } else {
                    for (int i = 1; i < 28; i++) { // - foodQuantity; i++) {
                        potionQuantityComboBox.getItems().add(i);
                    }

                    potionQuantityComboBox.getSelectionModel().select("5");
                }
            }

            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getFoodQuantityEvent() {
        return event -> {
            String foodQuantityString = getFoodQuantity();

            if (!foodQuantityString.equals("Select")) {
                int foodQuantity = Integer.valueOf(foodQuantityString);
                String potQuantityString = getPotionQuantityString();

                if (!potQuantityString.equals("")) {
                    int potQuantity = Integer.valueOf(potQuantityString);
                    if (getPotionSelected().equals("Sp Att & Str (4)")) {
                        potQuantity = potQuantity * 2;
                    }

                    if (foodQuantity + potQuantity >= 26)
                        promptText.setText("Please ensure that you are not exceeding inventory capacity.");
                     else
                        promptText.setText(" ");
                }
            } else {
                foodQuantityComboBox.getItems().clear();
            }

            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getPotionQuantityEvent() {
        return event -> {
            String potionQuantityString = getPotionQuantityString();

            if (!potionQuantityString.equals("Select")) {
                int potQuantity = Integer.valueOf(potionQuantityString);
                String foodQuantityString = getFoodQuantity();

                if (!foodQuantityString.equals("")) {
                    int foodQuantity = Integer.valueOf(foodQuantityString);
                    if (getPotionSelected().equals("Sp Att & Str (4)")) {
                        potQuantity = potQuantity * 2;
                    }

                    if (foodQuantity + potQuantity >= 26)
                        promptText.setText("Please ensure that you are not exceeding inventory capacity.");
                     else
                        promptText.setText(" ");
                }
            } else {
                potionQuantityComboBox.getItems().clear();
            }

            handleStartButton();
        };
    }

    private void handleStartButton() {
        String foodSelection = getFoodSelected();
        String potSelection = getPotionSelected();

        String foodQuantity = getFoodQuantity();
        String potionQuantity = getPotionQuantityString();

        startButton.setDisable(playstyleComboBox.getSelectionModel().getSelectedItem() == null ||
                (playstyleComboBox.getSelectionModel().getSelectedItem().toString().equals("Stand Still") && script.standStillCoord == null) ||
                (bankCheckBox.isSelected() && ((foodSelection.equals("Select") || (!foodSelection.equals("Select") && foodQuantity.equals("")))
                        && (potSelection.equals("Select") || (!potSelection.equals("Select") && potionQuantity.equals(""))))));
    }

    public EventHandler<ActionEvent> getStartButtonAction() {
        return event ->
        {
            if (crabTypeComboBox.getSelectionModel().getSelectedItem() != null && playstyleComboBox.getSelectionModel().getSelectedItem() != null) {
                switch (crabTypeComboBox.getSelectionModel().getSelectedItem().toString()) {
                    case "Rock Crab (Fremennik)":
                        script.crabName = "Rock Crab";
                        script.shellName = "Rocks";
                        script.crabArea = CrabAreas.FREM_ROCKCRAB_AREA;
                        script.spotLists = SpotAreas.rockCrabSpots;
                        script.refreshArea = CrabAreas.FREM_ROCKCRAB_REFRESH_AREA.getTiles();
                        script.botLocation = BotLocation.FremShore;
                        break;

                    case "Sand Crab (Zeah Shore)":
                        script.crabName = "Sand Crab";
                        script.shellName = "Sandy rocks";
                        script.crabArea = CrabAreas.ZEAH_SHORE_SANDCRAB_AREA;
                        script.spotLists = SpotAreas.sandCrabShoreSpots;
                        script.refreshArea = CrabAreas.ZEAH_SHORE_SANDCRAB_REFRESH_AREA.getTiles();
                        script.botLocation = BotLocation.ZeahShore;
                        break;

                    case "Sand Crab (Crab Island)":
                        script.crabName = "Sand Crab";
                        script.shellName = "Sandy rocks";
                        script.crabArea = CrabAreas.ZEAH_ISLAND_SANDCRAB_AREA;
                        script.spotLists = SpotAreas.sandCrabIslandSpots;
                        script.refreshArea = null;
                        script.botLocation = BotLocation.CrabIsland;
                }

                switch (playstyleComboBox.getSelectionModel().getSelectedItem().toString()) {
                    case "Active":
                        script.playStyle = Playstyle.Active;
                        break;

                    case "Spot Hop":
                        script.playStyle = Playstyle.SpotJump;
                        break;

                    case "Stand Still":
                        script.playStyle = Playstyle.StandStill;
                        break;
                }

                String potion = potSelectionComboBox.getSelectionModel().getSelectedItem().toString();
                switch (potion) {
                    case "Sp Att (4)":
                        script.potionType = Potion.SP_ATT;
                        script.usingPotion = true;
                        break;
                    case "Sp Str (4)":
                        script.potionType = Potion.SP_STR;
                        script.usingPotion = true;
                        break;
                    case "Sp Att & Str (4)":
                        script.potionType = Potion.SP_ATT_STR;
                        script.usingPotion = true;
                        break;
                    case "Sp Combat (4)":
                        script.potionType = Potion.SP_COMBAT;
                        script.usingPotion = true;
                        break;
                    case "Ranged (4)":
                        script.potionType = Potion.RANGED;
                        script.usingPotion = true;
                        break;
                    case "Zamorak brew (4)":
                        script.potionType = Potion.ZAM;
                        script.usingPotion = true;
                        break;
                    case "Attack (4)":
                        script.potionType = Potion.ATT;
                        script.usingPotion = true;
                        break;
                    case "Strength (4)":
                        script.potionType = Potion.STR;
                        script.usingPotion = true;
                        break;
                    case "Att & Str (4)":
                        script.potionType = Potion.ATT_STR;
                        script.usingPotion = true;
                        break;
                    default:
                        script.usingPotion = false;
                }

                String food = foodSelectionComboBox.getSelectionModel().getSelectedItem().toString();
                if (!food.equals("Select")) {
                    script.foodName = food;
                    script.foodQuantity = getFoodQuantityAsInt();
                    script.usingFood = true;
                }

                if(xTextField.getText() != null)
                    script.standStillCoord = new Position(Integer.parseInt(xTextField.getText()), Integer.parseInt(yTextField.getText()), Integer.parseInt(planeTextField.getText()));

                script.stopIfNoPotion = outOfPotionsCheckBox.isSelected();

                script.banking = bankCheckBox.isSelected();

                script.worldHop = worldHopCheckBox.isSelected() && !worldHopCheckBox.isDisable();

                script.pickupThrownEquipment = pickupThrownCheckBox.isSelected();

                script.potionQuantity = getPotionQuantityAsInt();

                script.useSpecial = useSpecialCheckBox.isSelected();

                super.getStart_BTEvent();

                startButton.textProperty().set("Update");
            } else {
                promptText.setText("Please select your preferred playstyle and crabtype.");
            }
        };
    }

    private String getFoodSelected() {
        Object selectedObj = foodSelectionComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null)
            return selectedObj.toString();
        else
            return "Select";
    }

    private String getPotionSelected() {
        Object selectedObj = potSelectionComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null)
            return selectedObj.toString();
        else
            return "Select";
    }

    private String getFoodQuantity() {
        Object selectedObj = foodQuantityComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null)
            return selectedObj.toString();
        else
            return "";
    }

    private String getPotionQuantityString() {
        Object selectedObj = potionQuantityComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null) {
            String quantity = selectedObj.toString();

            if (quantity.contains("each"))
                return quantity.substring(0, quantity.indexOf(" "));
            else
                return quantity;
        } else
            return "";
    }

    private int getPotionQuantityAsInt() {
        Object selectedObj = potionQuantityComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null) {
            String quantity = selectedObj.toString();

            if (quantity.contains("each"))
                return Integer.valueOf(quantity.substring(0, quantity.indexOf(" "))) * 2;
            else
                return Integer.valueOf(quantity);
        } else
            return -1;
    }

    private int getFoodQuantityAsInt() {
        Object selectedObj = foodQuantityComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null) {
            String quantity = selectedObj.toString();

            return Integer.valueOf(quantity);
        } else
            return -1;
    }
}