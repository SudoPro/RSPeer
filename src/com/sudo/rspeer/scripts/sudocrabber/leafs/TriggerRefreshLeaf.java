package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.enums.BotLocation;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

import java.util.List;

public class TriggerRefreshLeaf extends LeafTask {
    private SudoCrabber script;

    public TriggerRefreshLeaf(SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Crab no longer aggressive, refreshing zone");

        //if(script.botLocation == BotLocation.CrabIsland) {
            List<Position> positionList = script.botLocation == BotLocation.CrabIsland ? script.crabArea.getTiles() : Areas.radiusRectangle(script.player, 60).getTiles();
            Area areaAroundPlayer = Areas.radiusRectangle(script.player.getPosition(), 30);
            if(areaAroundPlayer != null) {
                positionList.removeAll(areaAroundPlayer.getTiles());

                for(int i = positionList.size() - 1; i > 0; i--){
                    if(!positionList.get(i).isPositionWalkable())
                        positionList.remove(i);
                }

                script.refreshArea = positionList;
            }
        //}

        script.resetRun = true;
        script.crabShellCheckCount = 0;
    }
}
