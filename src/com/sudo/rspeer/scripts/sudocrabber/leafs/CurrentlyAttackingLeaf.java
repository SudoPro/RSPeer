package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.commons.Time;

public class CurrentlyAttackingLeaf extends LeafTask {
    private SudoCrabber script;

    public CurrentlyAttackingLeaf(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Currently Attacking");
        script.crabShellCheckCount = 0;
            Time.sleep(1000, 2000);
    }
}