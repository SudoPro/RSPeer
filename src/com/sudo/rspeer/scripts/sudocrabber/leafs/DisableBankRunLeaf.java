package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.component.Bank;


public class DisableBankRunLeaf extends LeafTask {

    private SudoCrabber script;

    public DisableBankRunLeaf(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public void execute() {
        if(Bank.isOpen()){
            script.setCurrentTask("Closing bank");
            Bank.close();
        }else {
            script.setCurrentTask("Successfully replenished inventory");
            script.bankRun = false;
        }
    }
}