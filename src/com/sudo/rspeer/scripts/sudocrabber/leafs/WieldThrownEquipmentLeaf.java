package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.component.tab.Inventory;

public class WieldThrownEquipmentLeaf extends LeafTask {
    private SudoCrabber script;
    private Item equipment;

    public WieldThrownEquipmentLeaf (SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        equipment = Inventory.getFirst(i -> i.getName().equals(script.thrownEquipmentName));

        if(equipment != null){
            script.setCurrentTask("Wielding thrown equipment");
            equipment.interact("Wield");
        }
    }
}
