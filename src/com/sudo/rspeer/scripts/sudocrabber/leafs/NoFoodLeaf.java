package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;

public class NoFoodLeaf extends LeafTask {
    private SudoCrabber script;

    public NoFoodLeaf(SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Script out of edible food. Stopping script.");
        script.setStopping(true);
    }
}
