package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.enums.Playstyle;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Npcs;

public class CrabNullLeaf extends LeafTask {
    private SudoCrabber script;

    public CrabNullLeaf(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public void execute() {
        if (script.playStyle == Playstyle.StandStill) {
            script.setCurrentTask("Waiting for respawn...");
            Time.sleep(1000, 2000);
        } else {
            Npc crabShell = Npcs.getNearest(i -> i.getName().equals(script.shellName) && script.crabArea.contains(i));

            if (crabShell != null) {
                script.setCurrentTask("Traveling to nearest crab shell");
                Movement.walkTo(crabShell);
                Time.sleepWhile(script.player::isMoving, 750, 1500);
            } else {
                script.setCurrentTask("Traveling to crab area");
                Movement.walkTo(Areas.getRandomPosition(script.crabArea));
            }
        }
    }
}