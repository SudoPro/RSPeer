package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.commons.Time;

public class CheckShellFailSafeLeaf extends LeafTask {
    private SudoCrabber script;

    public CheckShellFailSafeLeaf(SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.crabShellCheckCount++;
        Time.sleep(300, 700);
    }
}
