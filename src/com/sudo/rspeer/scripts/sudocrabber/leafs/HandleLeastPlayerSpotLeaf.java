package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;


public class HandleLeastPlayerSpotLeaf extends LeafTask {
    private SudoCrabber script;

    public HandleLeastPlayerSpotLeaf(SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        if (script.leastPlayerSpot.size() > 0)
        {
            script.setCurrentTask("Traveling to least active crab spot");
            Position pos = script.leastPlayerSpot.get((int)(Math.random() * script.leastPlayerSpot.size()));
            Movement.walkTo(pos);
        }
        else
        {
            script.setCurrentTask("Traveling to available crab spot");
            Movement.walkTo(Areas.getRandomPosition(script.getRandomSpotArea()));
        }
    }
}
