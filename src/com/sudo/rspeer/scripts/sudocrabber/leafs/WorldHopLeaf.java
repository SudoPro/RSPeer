package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.Worlds;
import org.rspeer.runetek.api.component.WorldHopper;

public class WorldHopLeaf extends LeafTask {
    private SudoCrabber script;

    public WorldHopLeaf(SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        // Hop to a random members world
        WorldHopper.hopTo(Worlds.get(i -> !i.isBounty() && !i.isHighRisk() && i.isMembers() &&
            !i.isLastManStanding() && !i.isPVP() && !i.isSkillTotal()));
    }
}
