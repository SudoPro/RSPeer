package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.game.hud.SudoInventory;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.enums.BotLocation;
import com.sudo.rspeer.scripts.sudocrabber.enums.Potion;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RefillInventoryLeaf extends LeafTask {

    private SudoCrabber script;

    public RefillInventoryLeaf(SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        // Get items we need and withdraw them from bank
        int inventoryItemSize = 0;

        List<String> fullInventoryList = new ArrayList<String>(){{
            add(script.foodName);
            if(script.thrownEquipmentName != null && !script.thrownEquipmentName.equals(""))
                add(script.thrownEquipmentName);
            if(script.potionType != null)
                addAll(Arrays.asList(script.potionType.getPotionList()));
            if(script.botLocation == BotLocation.CrabIsland)
                add("Coins");
        }};

        if(Inventory.contains("Coins")){
            inventoryItemSize = Inventory.getCount(true, "Coins");
        }
        if(Inventory.contains("Vial")){
            script.setCurrentTask("Depositing Vial(s)");
            Bank.deposit("Vial", 0);
        }
        else if(SudoInventory.containsAnyExcept(fullInventoryList.toArray(new String[0]))){
            script.setCurrentTask("Depositing other items");
            Bank.depositAllExcept(fullInventoryList.toArray(new String[0]));
        }else if(script.botLocation == BotLocation.CrabIsland && inventoryItemSize  < 10000)
        {
            if (Bank.contains("Coins")) {
                script.setCurrentTask("Withdrawing Coins");
                if(Bank.withdraw("Coins", 10000 - inventoryItemSize))
                    Time.sleep(500);
            } else {
                script.setCurrentTask("Bank is out of Coins, stopping execution since script cannot enter island.");
                script.setStopping(true);
            }
        } else if(script.usingFood && (inventoryItemSize = Inventory.getItems(i -> i.getName().equals(script.foodName)).length) < script.foodQuantity) {
            if (Bank.contains(script.foodName)) {
                script.setCurrentTask("Withdrawing " + (script.foodQuantity - inventoryItemSize) + " " + script.foodName);
                Item item = Bank.getFirst(i -> i.getName().equals(script.foodName) && i.getStackSize() > 0);
                if(item != null)
                    if(Bank.withdraw(item.getName(), script.foodQuantity - inventoryItemSize))
                        Time.sleep(500);
            } else {
                script.setCurrentTask("Bank is out of " + script.foodName + ", stopping script.");
                script.setStopping(true);
            }
        } else if(script.usingPotion) {
            if (script.potionType == Potion.SP_ATT_STR) {
                if(Inventory.getItems(i -> i.getName().matches(Potion.SP_ATT.getPatternAsString())).length < script.potionQuantity / 2 ||  Inventory.getItems(i -> i.getName().matches(Potion.SP_STR.getPatternAsString())).length < script.potionQuantity / 2) {
                    if ((inventoryItemSize = Inventory.getItems(i -> i.getName().matches(Potion.SP_ATT.getPatternAsString())).length) < script.potionQuantity / 2) {
                        if (Bank.contains(Potion.SP_ATT.getPotionList())) {
                            // If you are using super attack and strength potions, we want to split the count between the two.
                            // Ex. If you have 10 potions, 5 should be att, 5 str. If you bank and you have 4 att and 5 str, you need to withdraw 1 att, which would be: (total count of potions / 2) - inventory count of that potion
                            Item item = Bank.getFirst(i -> i.getName().matches(Potion.SP_ATT.getPatternAsString()));
                            if (item != null) {
                                Debug.log("itemName = " + item.getDefinition().getName());
                                Debug.log("itemQuantity = " + item.getStackSize());
                                script.setCurrentTask("Withdrawing Super Attack potion(s)");
                                if(Bank.withdraw(item.getName(), (script.potionQuantity / 2) - inventoryItemSize))
                                    Time.sleep(1000);
                            }
                        } else {
                            if (!script.stopIfNoPotion) {
                                script.setCurrentTask("Bank is out of Super Attack potions, we are no longer using potions.");
                                script.usingPotion = false;
                            } else {
                                script.setCurrentTask("Bank is out of Sp Attack potions, stopping script per user settings.");
                                script.setStopping(true);
                            }
                        }
                    } else if ((inventoryItemSize = Inventory.getItems(i -> i.getName().matches(Potion.SP_STR.getPatternAsString())).length) < script.potionQuantity / 2) {
                        if (Bank.contains(Potion.SP_STR.getPotionList())) {
                            Item item = Bank.getFirst(i -> i.getName().matches(Potion.SP_STR.getPatternAsString()));
                            if(item != null) {
                                Debug.log("itemName = " + item.getDefinition().getName());
                                Debug.log("itemQuantity = " + item.getStackSize());
                                script.setCurrentTask("Withdrawing Super Strength potion(s)");
                                if(Bank.withdraw(item.getName(), (script.potionQuantity / 2) - inventoryItemSize))
                                    Time.sleep(1000);
                            }
                        } else {
                            if (!script.stopIfNoPotion) {
                                script.setCurrentTask("Bank is out of Super Strength potions, we are no longer using potions.");
                                script.usingPotion = false;
                            } else {
                                script.setCurrentTask("Bank is out of Sp Strength potions, stopping script per user settings.");
                                script.setStopping(true);
                            }
                        }
                    }
                }
            }
            else if (script.potionType == Potion.ATT_STR) {
                if(Inventory.getItems(i -> i.getName().matches(Potion.ATT.getPatternAsString())).length < script.potionQuantity / 2 || Inventory.getItems(i -> i.getName().matches(Potion.STR.getPatternAsString())).length < script.potionQuantity / 2) {
                    if ((inventoryItemSize = Inventory.getItems(i -> i.getName().matches(Potion.ATT.getPatternAsString())).length) < script.potionQuantity / 2) {
                        if (Bank.contains(Potion.ATT.getPotionList())) {
                            // If you are using super attack and strength potions, we want to split the count between the two.
                            // Ex. If you have 10 potions, 5 should be att, 5 str. If you bank and you have 4 att and 5 str, you need to withdraw 1 att, which would be: (total count of potions / 2) - inventory count of that potion
                            Item item = Bank.getFirst(i -> i.getName().matches(Potion.ATT.getPatternAsString()));
                            if (item != null) {
                                script.setCurrentTask("itemName = " + item.getDefinition().getName());
                                script.setCurrentTask("itemQuantity = " + item.getStackSize());
                                script.setCurrentTask("Withdrawing Attack potion(s)");
                                if(Bank.withdraw(item.toString(), (script.potionQuantity / 2) - inventoryItemSize))
                                    Time.sleep(1000);
                            }
                        } else {
                            if (!script.stopIfNoPotion) {
                                script.setCurrentTask("Bank is out of Attack potions, we are no longer using potions.");
                                script.usingPotion = false;
                            } else {
                                script.setCurrentTask("Bank is out of Attack potions, stopping script per user settings.");
                                script.setStopping(true);
                            }
                        }
                    } else if ((inventoryItemSize = Inventory.getItems(i -> i.getName().matches(Potion.STR.getPatternAsString())).length) < script.potionQuantity / 2) {
                        if (Bank.contains(Potion.STR.getPotionList())) {
                            Item item = Bank.getFirst(i -> i.getName().matches(Potion.STR.getPatternAsString()));
                            if(item != null) {
                                script.setCurrentTask("itemName = " + item.getDefinition().getName());
                                script.setCurrentTask("itemQuantity = " + item.getStackSize());
                                script.setCurrentTask("Withdrawing Super Strength potion(s)");
                                if(Bank.withdraw(item.getName(), (script.potionQuantity / 2) - inventoryItemSize))
                                    Time.sleep(1000);
                            }
                        } else {
                            if (!script.stopIfNoPotion) {
                                script.setCurrentTask("Bank is out of Strength potions, we are no longer using potions.");
                                script.usingPotion = false;
                            } else {
                                script.setCurrentTask("Bank is out of Strength potions, stopping script per user settings.");
                                script.setStopping(true);
                            }
                        }
                    }
                }
            } else{
                if((inventoryItemSize = Inventory.getItems(i -> i.getName().matches(script.potionType.getPatternAsString())).length) < script.potionQuantity) {
                    if (Bank.contains(script.potionType.getPotionList())) {
                        // If you are using super attack and strength potions, we want to split the count between the two.
                        // Ex. If you have 10 potions, 5 should be att, 5 str. If you bank and you have 4 att and 5 str, you need to withdraw 1 att, which would be: (total count of potions / 2) - inventory count of that potion
                        Item item = Bank.getFirst(i -> Arrays.asList(script.potionType.getPotionList()).contains(i.getName()));
                        if(item != null) {
                            script.setCurrentTask("itemName = " + item.getDefinition().getName());
                            script.setCurrentTask("itemQuantity = " + item.getStackSize());
                            script.setCurrentTask("Withdrawing " + script.potionType.getPotionName() + " potion(s)");
                            if(Bank.withdraw(item.getName(), script.potionQuantity - inventoryItemSize))
                                Time.sleep(1000);
                        }
                    } else {
                        if (script.stopIfNoPotion) {
                            script.setCurrentTask("Bank is out of " + script.potionType.getPotionName() + " potions, stopping script per user settings.");
                            script.setStopping(true);
                        } else {
                            script.setCurrentTask("Bank is out of " + script.potionType.getPotionName() + " potions, we are no longer using potions.");
                            script.usingPotion = false;
                        }
                    }
                }
            }
        }
    }
}
