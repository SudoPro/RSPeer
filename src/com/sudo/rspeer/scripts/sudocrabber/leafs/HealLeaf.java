package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;

public class HealLeaf extends LeafTask {
    private SudoCrabber script;

    public HealLeaf(SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Health tolerance exceeded, eating food...");
        if(script.food.interact("Eat")){
            script.healPercent = (script.userProvidedPercent - 7.5) + (Math.random() * 15);
            script.setCurrentTask("Eating successful, new health tolerance is " + script.healPercent);
        }
    }
}
