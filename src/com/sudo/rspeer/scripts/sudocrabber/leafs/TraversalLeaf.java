package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.areas.TraversalAreas;
import com.sudo.rspeer.scripts.sudocrabber.enums.BotLocation;
import com.sudo.rspeer.scripts.sudocrabber.enums.Playstyle;
import com.sudo.rspeer.scripts.sudocrabber.enums.TraversalLocation;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Npcs;

import java.util.Arrays;

public class TraversalLeaf extends LeafTask {
    private SudoCrabber script;

    public TraversalLeaf(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public void execute() {
        if (script.traversalLocation == TraversalLocation.crabSpot) {
            if (script.botLocation == BotLocation.CrabIsland && !script.crabArea.contains(script.player)) {
                enterCrabIsland();
            } else {
                script.setCurrentTask("Traveling to crab spot");
                if (script.playStyle == Playstyle.StandStill)
                    Movement.walkTo(script.standStillCoord);
                else
                    Movement.walkTo(Areas.getRandomPosition(script.getRandomSpotArea()));
            }
        } else if (script.traversalLocation == TraversalLocation.crabArea) {
            if (script.botLocation == BotLocation.CrabIsland && !script.crabArea.contains(script.player)) {
                enterCrabIsland();
            } else {
                script.setCurrentTask("Traversing to general crab area");
                Movement.walkTo(Areas.getRandomPosition(script.crabArea));
            }
        } else if (script.traversalLocation == TraversalLocation.crabSpotArea) {
            if (script.botLocation == BotLocation.CrabIsland && !script.crabArea.contains(script.player)) {
                enterCrabIsland();
            } else {
                script.setCurrentTask("Traversing to a nearby crab");
                Npc[] npcs = Npcs.getLoaded(i -> i.getName().equals(script.shellName) && script.crabArea.contains(i));
                if(npcs != null && npcs.length > 0) {
                    script.crab = npcs[Random.nextInt(0, npcs.length - 1)];
                    Movement.walkTo(script.crab);
                }
            }
        } else if (script.traversalLocation == TraversalLocation.refreshArea) {
            script.setCurrentTask("Traveling to refresh area");
            Movement.walkTo(Areas.getRandomPosition(script.randomRefreshArea));
        } else if (script.traversalLocation == TraversalLocation.bankArea) {
            if (script.botLocation == BotLocation.CrabIsland && script.crabArea.contains(script.player)) {
                npcLeaveIslandInteract();
            } else {
                script.setCurrentTask("Traveling to bank");
                Movement.walkTo(Areas.getRandomPosition(TraversalAreas.ZEAH_BANK_AREA));
            }
        }

        // Delay after each attempt to traverse
        Time.sleep(500, 1500);
        Time.sleepWhile(script.player::isMoving, 1000, 1500);
    }

    private void enterCrabIsland() {
        if (TraversalAreas.ZEAH_CRAB_ISLAND_GOTO_AREA.contains(script.player)) {
            npcEnterIslandInteract();
        } else {
            script.setCurrentTask("Traveling to boat guy");
            Movement.walkTo(Areas.getRandomPosition(TraversalAreas.ZEAH_CRAB_ISLAND_GOTO_AREA));
        }
    }

    private void npcLeaveIslandInteract() {
        script.islandNpc = Npcs.getNearest(i -> Arrays.asList(i.getActions()).contains("Quick-travel"));

        if (script.islandNpc != null && script.islandNpc.distance(script.player) < 10) {
            script.setCurrentTask("Attempting to leave island");
            script.islandNpc.interact("Quick-travel");
        } else {
            script.setCurrentTask("Traveling to boat guy");
            Movement.walk(Areas.getRandomPosition(TraversalAreas.ZEAH_CRAB_ISLAND_LEAVE_AREA));
        }
    }

    private void npcEnterIslandInteract() {
        int coinQuantity = Inventory.getCount(true,"Coins");
        script.islandNpc = Npcs.getNearest(i -> Arrays.asList(i.getActions()).contains("Quick-travel"));
        Debug.log("coinQuantity: " + coinQuantity);

        if (coinQuantity < 10000) {
            // Delay for a few seconds or until we are on the island. If we delay the full amount and still are not on the island, then trigger bank run.
            if(!Time.sleepUntil(() -> script.crabArea.contains(script.player), 2000, 5000)) {
                script.setCurrentTask("FailSafe: Banking didn't withdraw coins, restarting bank task.");
                script.bankRun = true;
            }
        } else {
            if (script.islandNpc != null && script.islandNpc.distance(script.player) < 10) {
                script.setCurrentTask("Attempting to enter island");
                if(script.islandNpc.interact("Quick-travel"))
                    Time.sleepUntil(() -> script.crabArea.contains(script.player), 1000, 5000);
            } else {
                script.setCurrentTask("Traveling to boat guy");
                Movement.walkTo(Areas.getRandomPosition(TraversalAreas.ZEAH_CRAB_ISLAND_GOTO_AREA));
            }
        }
    }
}