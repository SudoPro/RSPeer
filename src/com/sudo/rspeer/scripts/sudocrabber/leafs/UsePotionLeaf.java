package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.enums.Potion;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;

import java.util.Arrays;

public class UsePotionLeaf extends LeafTask {
    private SudoCrabber script;

    public UsePotionLeaf (SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        Item item = null;

        if(script.potionType == Potion.SP_ATT_STR){
            if(Potion.SP_ATT.areSkillsLow())
                item = Inventory.getFirst(i -> i.getName().matches(Potion.SP_ATT.getPatternAsString()));
            else
                item = Inventory.getFirst(i -> i.getName().matches(Potion.SP_STR.getPatternAsString()));
        }else if(script.potionType == Potion.ATT_STR){
            if(Potion.SP_ATT.areSkillsLow())
                item = Inventory.getFirst(i -> i.getName().matches(Potion.ATT.getPatternAsString()));
            else
                item = Inventory.getFirst(i -> i.getName().matches(Potion.STR.getPatternAsString()));
        }else
             item = Inventory.getFirst(i -> Arrays.asList(i.getActions()).contains("Drink"));


        if(item != null){
            script.setCurrentTask("Attempting to drink potion");
            if(item.interact("Drink")){
                Time.sleep(500, 1000);
            }
        }
    }
}
