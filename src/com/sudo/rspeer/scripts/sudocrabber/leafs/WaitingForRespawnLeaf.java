package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.commons.Time;

public class WaitingForRespawnLeaf extends LeafTask {
    private SudoCrabber script;

    public WaitingForRespawnLeaf(SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Waiting for respawn...");
        Time.sleep(1000, 3000);
    }
}
