package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.game.entities.SudoPlayer;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.commons.Time;

public class AttackCrabLeaf extends LeafTask {
    private SudoCrabber script;

    public AttackCrabLeaf(SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Attempting to attack crab");

        if (script.crab.interact("Attack")) {
            Time.sleepUntil(() -> SudoPlayer.isTargetting(script.crabName), 750, 1500);
            Time.sleep(500, 1000);
        }
    }
}
