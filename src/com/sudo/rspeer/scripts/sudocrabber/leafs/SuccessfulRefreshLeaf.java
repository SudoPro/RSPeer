package com.sudo.rspeer.scripts.sudocrabber.leafs;

import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;

public class SuccessfulRefreshLeaf extends LeafTask {
    private SudoCrabber script;

    public SuccessfulRefreshLeaf(SudoCrabber script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Successful area refresh");
        script.resetRun = false;
    }
}
