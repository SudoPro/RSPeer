package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.enums.Playstyle;

public class IsActivePlaystyle extends BranchTask {
    private SudoCrabber script;

    public IsActivePlaystyle(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public boolean validate() {
        validate = script.playStyle == Playstyle.Active;
        Debug.log("Branch: IsActivePlaystyle: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.attackBranch;
    }

    @Override
    public TreeTask failureTask() {
        return script.isAnotherOnSpot;
    }
}