package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.game.entities.SudoPlayer;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;

public class IsAttacking extends BranchTask {
    private SudoCrabber script;

    public IsAttacking(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public boolean validate() {
        validate = SudoPlayer.isTargetting(script.crabName);
        Debug.log("Branch: IsAttacking: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.currentlyAttackingLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return script.isCrabShellNull;
    }
}