package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.component.tab.Inventory;

public class InventoryHasEquipment extends BranchTask {
    private SudoCrabber script;

    public InventoryHasEquipment (SudoCrabber script){
        this.script = script;
    }

    @Override
    public boolean validate() {
        validate = Inventory.contains(script.thrownEquipmentName);
        Debug.log("Branch: InventoryHasEquipment: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.wieldThrownEquipmentLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return script.areSkillsLow;
    }
}
