package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.SudoRootTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.game.entities.SudoPlayer;
import com.sudo.rspeer.api.skills.model.XPInfo;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.areas.TraversalAreas;
import com.sudo.rspeer.scripts.sudocrabber.enums.Playstyle;
import com.sudo.rspeer.scripts.sudocrabber.enums.TraversalLocation;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.*;
import org.rspeer.runetek.api.local.Health;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public class Root extends SudoRootTask {
    private SudoCrabber script;
    private EquipmentSlot equipment;

    public Root(SudoCrabber script) {
        super(script);
        this.script = script;
    }


    @Override
    public void updateDisplayMap() {

    }

    @Override
    public TreeTask rootTask() {
        if (script.firstLogin) {
            if (Game.isLoggedIn()) {
                // Add Skills to track to the HashMap
                script.xpInfoMap.put(Skill.ATTACK, new XPInfo(Skill.ATTACK));
                script.xpInfoMap.put(Skill.STRENGTH, new XPInfo(Skill.STRENGTH));
                script.xpInfoMap.put(Skill.DEFENCE, new XPInfo(Skill.DEFENCE));
                script.xpInfoMap.put(Skill.HITPOINTS, new XPInfo(Skill.HITPOINTS));
                script.xpInfoMap.put(Skill.MAGIC, new XPInfo(Skill.MAGIC));
                script.xpInfoMap.put(Skill.RANGED, new XPInfo(Skill.RANGED));

                script.firstLogin = false;

                Debug.log("Location: " + script.botLocation.name());
                Debug.log("PlayStyle: " + script.playStyle);
                Debug.log("HP Level: " + Skills.getCurrentLevel(Skill.HITPOINTS));
                Debug.log("Heal Percent: " + script.userProvidedPercent);
                Debug.log("Using Food: " + script.usingFood);
                Debug.log("Food Type: " + script.foodName);
                Debug.log("Food Quantity: " + script.foodQuantity);
                Debug.log("Using Potions: " + script.usingPotion);
                Debug.log("Potion Type: " + script.potionType.name());
                Debug.log("Potion Quantity: " + script.potionQuantity);
                Debug.log("Stop Out Of Potions: " + script.stopIfNoPotion);
                Debug.log("Pickup Thrown Equipment: " + script.pickupThrownEquipment);
                Debug.log("Player Run Count: " + script.playerRunCount);
                Debug.log("World Hop: " + script.worldHop);


            }
        }

        if (script.player != null) {
            if(script.thrownEquipmentName.equals("")){
                if((equipment = EquipmentSlot.MAINHAND) != null){
                    script.thrownEquipmentName = equipment.getItemName();
                }
            }

            //Debug.log(String.valueOf(script.thrownPickupTimer.getRemainingTimeInSeconds()));

            if (validate = script.banking && script.bankRun) {
                Debug.log("Root Branch: ShouldBank: " +  validate);
                return script.isInBankArea;
            } else {
                validate = script.banking && script.isRefillInventoryRequired();
                Debug.log("Root Branch: Refill Required: " + validate);
                if (validate) {
                    script.setCurrentTask("Inventory refill required. Running to bank.");
                    script.bankRun = true;
                } else {
                    validate = Health.getPercent() > script.healPercent;
                    Debug.log("Root Branch: IsHealingNeeded: " + validate);
                    Debug.log("ActualHealthPercent: " + Health.getPercent());
                    Debug.log("GeneratedHealthPercent: " + script.healPercent);
                    if (validate) {
                        // First determine whether the user is on the PRO or Lite version of the script. If on the Lite version, bot must stop after 10 minutes of use.
                        //if (script.getMetaData().getHourlyPrice().doubleValue() > 0 || script.isValidSession() || Environment.isSDK()) {
                        if (validate = script.player.distance(script.crabArea.getCenter()) > 250) {
                            Debug.log("Root Branch: Outside Botting Area: " + validate);
                            script.setCurrentTask("Please start the bot within the supported areas. Check the Bot's Overview page for more information.");
                            script.setPaused(true);
                        } else if (validate = script.resetRun) {
                            Debug.log("Root Branch: ResetRun: " + validate);
                            if ((validate = script.refreshArea != null && script.refreshArea.contains(script.player.getPosition())) || (script.randomRefreshArea != null && script.randomRefreshArea.contains(script.player.getPosition()))) {
                                Debug.log("Root Branch: Refresh Contains Player: " + validate);
                                script.randomRefreshArea = null;
                                return script.successfulRefreshLeaf;
                            } else {
                                Debug.log("Root Branch: Refresh Contains Player: " + validate);
                                if (script.randomRefreshArea == null) {
                                    if (script.refreshArea != null) {
                                        Position tempPosition = script.refreshArea.get(Random.nextInt(0, script.refreshArea.size() - 1));
                                        script.randomRefreshArea = Area.rectangular(tempPosition.getX() - 5, tempPosition.getY() - 5, tempPosition.getX() + 5, tempPosition.getY() + 5);
                                    } else
                                        return script.triggerRefreshLeaf;
                                }
                                script.traversalLocation = TraversalLocation.refreshArea;
                                return script.traversalLeaf;
                            }
                        } else if (validate = !script.crabArea.contains(script.player)) {
                            Debug.log("Root Branch: CrabArea!ContainsPlayer: " + validate);
                            script.traversalLocation = script.playStyle == Playstyle.Active ? TraversalLocation.crabArea : TraversalLocation.crabSpot;
                            return script.traversalLeaf;
                        } else if (validate = !script.isPlayerOnCrabSpot() && script.playStyle == Playstyle.StandStill && !script.pickingUpThrown && !script.isAttacking.validate()) {
                            Debug.log("Root Branch: OffSpotCoord: " + validate);
                            script.traversalLocation = TraversalLocation.crabSpot;
                            return script.traversalLeaf;
                        } else if (validate = !Inventory.isFull() && script.pickupThrownEquipment && script.thrownPickupTimer.hasExpired()) {
                            Debug.log("Root Branch: ReadyForThrownPickup: " + validate);
                            return script.isThrownEquipmentAround;
                        } else if (validate = readyToUseSpecial()) {
                            Debug.log("Root Branch: ReadyToUseSpecial: " + validate);
                            return script.useSpecialAttackLeaf;
                        } else {
                            if(script.useSpecial && script.checkSpecialTimer.hasExpired() && Combat.getSpecialEnergy() > 99 && script.isAttacking.validate() && !TraversalAreas.ZEAH_BANK_AREA.contains(script.player))
                                return script.useSpecialAttackLeaf;
                            else
                                return script.inventoryHasEquipment;
                        }
//                    } else {
//                        return script.liteVersionExpireLeaf;
//                    }
                    } else {
                        return script.healBranch;
                    }
                }
            }
        }
        return script.playerNullLeaf;
    }

    private boolean readyToUseSpecial(){
        if(script.useSpecial)
            if(script.checkSpecialTimer.hasExpired())
                if(!TraversalAreas.ZEAH_BANK_AREA.contains(script.player))
                    if(script.refreshArea != null)
                        if(!script.refreshArea.contains(script.player.getPosition()))
                            if(SudoPlayer.isTargetting(script.crabName))
                                return true;

        return false;
    }
}
