package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;

public class DoesAvailableSpotContainPlayer extends BranchTask {
    private SudoCrabber script;

    public DoesAvailableSpotContainPlayer(SudoCrabber script){
        this.script = script;
    }

    @Override
    public boolean validate() {
        validate = !script.leastPlayerSpot.contains(script.player.getPosition());
        Debug.log("Branch: AvailableSpotContainPlayer: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.handleLeastPlayerSpotLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return script.attackBranch;
    }
}
