package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;

public class AreSkillsLow extends BranchTask {

    private SudoCrabber script;

    public AreSkillsLow(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public boolean validate() {
        validate = script.usingPotion && script.crabArea.contains(script.player) && script.potionType.areSkillsLow();
        Debug.log("Branch: AreSkillsLow: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.usePotionLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return script.isAttacking;
    }
}