package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.scene.Npcs;

public class IsCrabShellNull extends BranchTask
{
    private SudoCrabber script;

    public IsCrabShellNull(SudoCrabber script){
        this.script = script;
    }

    @Override
    public boolean validate()
    {
        // Crab shell around the user
        script.crabShell = Npcs.getNearest(i -> i.getName().contains(script.shellName) &&
            script.crabArea.contains(i) && Areas.radiusRectangle(script.player, 3).contains(script.player));

        validate = script.crabShell != null;
        Debug.log("Branch: IsCrabShell!Null: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        Debug.log("Branch: CrabShellNearby(" + script.crabShellCheckCount + "): " + (script.player.distance(script.crabShell.getPosition()) < 2));

        if (script.player.distance(script.crabShell.getPosition()) < 2) {
            // We want to check 3 times if the shell is around us and not turning into an attackable crab
            if (script.crabShellCheckCount >= 3) {
                return script.triggerRefreshLeaf;
            } else {
                return script.checkShellFailSafeLeaf;
            }
        } else
            return script.attackBranch;
    }

    @Override
    public TreeTask failureTask()
    {
        return script.isActivePlaystyle;
    }
}
