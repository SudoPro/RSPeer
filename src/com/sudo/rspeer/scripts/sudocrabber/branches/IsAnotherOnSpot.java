package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.enums.Playstyle;
import org.rspeer.runetek.api.scene.Players;

public class IsAnotherOnSpot extends BranchTask {
    private SudoCrabber script;

    public IsAnotherOnSpot(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public boolean validate() {
        validate = Players.getLoaded(i -> !i.getName().equals(script.player.getName()) && Areas.radiusRectangle(script.player, 3).contains(i)).length > script.playerRunCount && script.playStyle != Playstyle.Active;
        Debug.log("Branch: IsAnotherOnSpot: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.isStandStillPlaystyle;
    }

    @Override
    public TreeTask failureTask() {
        return script.attackBranch;
    }
}