package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.scene.Npcs;

public class AttackBranch extends BranchTask {
    private SudoCrabber script;

    public AttackBranch(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public boolean validate() {
        script.crab = Npcs.getNearest(i -> i.getName().contains(script.crabName) &&
                Areas.radiusRectangle(script.player, 2).contains(i.getPosition()) &&
                script.crabArea.contains(i.getPosition()));

        validate = script.crab != null;
        Debug.log("Branch: AttackBranch: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {

        return script.attackCrabLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return script.crabNullLeaf;
    }
}