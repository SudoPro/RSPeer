package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.enums.TraversalLocation;
import org.rspeer.runetek.api.component.tab.Inventory;

import java.util.Arrays;

public class HealBranch extends BranchTask {
    private SudoCrabber script;

    public HealBranch(SudoCrabber script) {
        this.script = script;
    }


    @Override
    public boolean validate() {
        script.food = Inventory.getFirst(i -> Arrays.asList(i.getActions()).contains("Eat"));

        validate = script.food != null;
        Debug.log("Branch: HealBranch: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.healLeaf;
    }

    @Override
    public TreeTask failureTask() {
        if (script.refreshArea != null && script.refreshArea.contains(script.player.getPosition())) {
            return script.noFoodLeaf;
        } else {
            script.traversalLocation = TraversalLocation.refreshArea;
            return script.traversalLeaf;
        }
    }
}