package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.game.entities.Areas;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.adapter.Positionable;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;

import java.util.Arrays;
import java.util.List;

public class IsCrabSpotAvailable extends BranchTask {
    private SudoCrabber script;
    private List<Positionable> playerQuery;

    public IsCrabSpotAvailable(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public boolean validate() {
        int spotSize = 500;
        validate = false;

        // See if there are any spots where there are NOT multiple players already there.
        for (int i = 0; i < script.spotLists.size(); i++) {
            final List<Position> currentSpots = script.spotLists.get(i);
            // If the player count around the nth spot has a lower count than the player run count
            if ((playerQuery = Arrays.asList(Players.getLoaded(p -> Areas.radiusRectangle(currentSpots.get(0), 2).contains(p) &&
                    !p.getName().equals(script.player.getName())))).size() < script.playerRunCount) {

                validate = true;

                // If this spot's size is less than spotSize, then we have our current least populated spot
                if (playerQuery.size() < spotSize) {
                    script.leastPlayerSpot = script.spotLists.get(i);
                    spotSize = playerQuery.size();
                }
            }
        }

        Debug.log("Branch: IsCrabSpotAvailable: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.doesAvailableSpotContainPlayer;
    }

    @Override
    public TreeTask failureTask() {
        return script.doesAvailableSpotContainPlayer;
    }

}