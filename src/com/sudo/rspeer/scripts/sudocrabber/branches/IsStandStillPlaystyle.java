package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.enums.Playstyle;

public class IsStandStillPlaystyle extends BranchTask {
    private SudoCrabber script;

    public IsStandStillPlaystyle(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public boolean validate() {
        // All players around the user, other than the local player
        //LocatableEntityQueryResults<Player> playerList = Players.newQuery().names(Pattern.compile("^(?!.*(" + script._player.getName() + "))")).results();

        // Get nearest crab that is not attacking the user
        //script.crab = Npcs.newQuery().names(script.crabName).within(new Area.Circular(script._player.getPosition(), 3)).targeting(playerList.toArray()).results().nearest();

        validate = script.playStyle == Playstyle.StandStill;
        Debug.log("Branch: IsStandStillPlaystyle: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        if (script.worldHop) {
            // Hop to a random members world
            script.setCurrentTask("Multiple player(s) found, hopping to new world");
            return script.worldHopLeaf;
        } else
            return script.waitingForRespawnLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return script.isCrabSpotAvailable;
    }
}