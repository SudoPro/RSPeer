package com.sudo.rspeer.scripts.sudocrabber.branches;


import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.areas.TraversalAreas;
import com.sudo.rspeer.scripts.sudocrabber.enums.TraversalLocation;

public class IsInBankArea extends BranchTask {

    private SudoCrabber script;

    public IsInBankArea(SudoCrabber script){
        this.script = script;
    }

    @Override
    public boolean validate() {
        validate = TraversalAreas.ZEAH_BANK_AREA.contains(script.player);
        Debug.log("Branch: IsInBankArea: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.isRefillRequired;
    }

    @Override
    public TreeTask failureTask() {
        script.traversalLocation = TraversalLocation.bankArea;
        return script.traversalLeaf;
    }
}
