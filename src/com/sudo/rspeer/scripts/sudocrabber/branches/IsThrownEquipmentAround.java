package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.scene.Pickables;

public class IsThrownEquipmentAround extends BranchTask {
    private SudoCrabber script;

    public IsThrownEquipmentAround(SudoCrabber script) {
        this.script = script;
    }

    @Override
    public boolean validate() {
        script.thrownEquipments = Pickables.getLoaded(i -> i.getName().equals(script.thrownEquipmentName) && i.distance(script.player) < 4);

        validate = script.thrownEquipments != null && script.thrownEquipments.length > 0;
        Debug.log("Branch: IsThrownEquipmentAround: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        script.pickingUpThrown = true;
        script.nearestThrownEquipment = script.thrownEquipments[Random.nextInt(0, script.thrownEquipments.length - 1)];
        script.pickupThrownEquipmentLeaf.setInteractable(script.nearestThrownEquipment);
        script.pickupThrownEquipmentLeaf.setName(script.thrownEquipmentName);
        return script.pickupThrownEquipmentLeaf;
    }

    @Override
    public TreeTask failureTask() {
        script.pickingUpThrown = false;
        script.thrownPickupTimer.reset();
        return script.inventoryHasEquipment;
    }
}