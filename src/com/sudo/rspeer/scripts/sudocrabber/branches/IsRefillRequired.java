package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import com.sudo.rspeer.scripts.sudocrabber.enums.BotLocation;
import com.sudo.rspeer.scripts.sudocrabber.enums.Potion;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.component.tab.Inventory;

import java.util.Arrays;

public class IsRefillRequired extends BranchTask {

    private SudoCrabber script;

    public IsRefillRequired(SudoCrabber script){
        this.script = script;
    }

    @Override
    public boolean validate() {
        validate = isRefillRequired();
        Debug.log("Branch: IsRefillRequired: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.isBankOpen;
    }

    @Override
    public TreeTask failureTask() {
        return script.disableBankRunLeaf;
    }

    private boolean isRefillRequired(){
        Item[] foodResults = Inventory.getItems(i -> Arrays.asList(i.getActions()).contains("Eat"));
        int coinQuantity = Inventory.getCount(true, "Coins");

        if (script.potionType == Potion.SP_ATT_STR) {
            Item[] attackPotionResults = Inventory.getItems(i -> i.getName().matches(Potion.SP_ATT.getPatternAsString()));
            Item[] strengthPotionResults = Inventory.getItems(i -> i.getName().matches(Potion.SP_STR.getPatternAsString()));

            return (script.botLocation == BotLocation.CrabIsland && coinQuantity < 10000) || (script.usingFood && foodResults.length < script.foodQuantity) || (script.usingPotion && (attackPotionResults.length < script.potionQuantity / 2 || strengthPotionResults.length < script.potionQuantity / 2));
        }
        else if (script.potionType == Potion.ATT_STR) {
            Item[] attackPotionResults = Inventory.getItems(i -> i.getName().matches(Potion.ATT.getPatternAsString()));
            Item[] strengthPotionResults = Inventory.getItems(i -> i.getName().matches(Potion.STR.getPatternAsString()));

            return (script.botLocation == BotLocation.CrabIsland && coinQuantity < 10000) || (script.usingFood && foodResults.length < script.foodQuantity) || (script.usingPotion && (attackPotionResults.length < script.potionQuantity / 2 || strengthPotionResults.length < script.potionQuantity / 2));
        }else {
            Item[] potionResults = Inventory.getItems(i -> i.getName().matches(script.potionType.getPatternAsString()));

            return (script.botLocation == BotLocation.CrabIsland && coinQuantity < 10000) || (script.usingFood && foodResults.length  < script.foodQuantity) || (script.usingPotion && potionResults.length < script.potionQuantity);
        }
    }
}
