package com.sudo.rspeer.scripts.sudocrabber.branches;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.scripts.sudocrabber.SudoCrabber;
import org.rspeer.runetek.api.component.Bank;

public class IsBankOpen extends BranchTask {

    private SudoCrabber script;

    public IsBankOpen(SudoCrabber script){
        this.script = script;
    }

    @Override
    public boolean validate() {
        validate = Bank.isOpen();
        Debug.log("Branch: IsBankOpen: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return script.refillInventoryLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return script.openBankLeaf;
    }
}
