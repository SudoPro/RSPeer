package com.sudo.rspeer.scripts.sudocrabber;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.api.util.SudoTimer;
import com.sudo.rspeer.common.leafs.InteractLeaf;
import com.sudo.rspeer.scripts.sudocrabber.areas.CrabAreas;
import com.sudo.rspeer.scripts.sudocrabber.areas.SpotAreas;
import com.sudo.rspeer.scripts.sudocrabber.branches.*;
import com.sudo.rspeer.scripts.sudocrabber.enums.BotLocation;
import com.sudo.rspeer.scripts.sudocrabber.enums.Playstyle;
import com.sudo.rspeer.scripts.sudocrabber.enums.Potion;
import com.sudo.rspeer.scripts.sudocrabber.enums.TraversalLocation;
import com.sudo.rspeer.scripts.sudocrabber.leafs.*;
import com.sudo.rspeer.scripts.sudocrabber.ui.CrabFXController;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.event.types.ItemTableEvent;
import org.rspeer.script.ScriptMeta;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@ScriptMeta(developer = "Proxi", desc = "Slaughters Sand and Rock Crabs. AFK or Active playstyles. Supports worldhop, food, potion, banking, and more!", name = "SudoCrabber")
public class SudoCrabber extends SudoScript {

    public Area crabArea = CrabAreas.ZEAH_SHORE_SANDCRAB_AREA, randomRefreshArea = null;
    public List<Position> refreshArea = CrabAreas.ZEAH_SHORE_SANDCRAB_REFRESH_AREA.getTiles();
    public String crabName = "Sand Crab", shellName = "Sandy rocks", foodName = "", thrownEquipmentName = "";
    public Npc crab = null, crabShell = null, islandNpc = null;
    public SceneObject bankObj = null, bankDoorObj = null;
    public Pickable[] thrownEquipments;
    public Pickable nearestThrownEquipment;
    public int crabShellCheckCount = 0, playerRunCount = 2, potionQuantity = 5, foodQuantity = 10;
    public boolean worldHop = false, resetRun = false, banking = false, usingPotion = false, usingFood = false, bankRun = false, stopIfNoPotion = false, pickupThrownEquipment = true, pickingUpThrown = false;
    public Playstyle playStyle = Playstyle.SpotJump;
    public Potion potionType = Potion.SP_ATT_STR;
    public double healPercent = 50, userProvidedPercent = 50;
    public Item food;
    public TraversalLocation traversalLocation = TraversalLocation.crabArea;
    public BotLocation botLocation = BotLocation.ZeahShore;

    // An List of a list of Positions
    // This is a double list because we could have multiple Positions cover the same 'spots'
    public ArrayList<ArrayList<Position>> spotLists = SpotAreas.sandCrabShoreSpots;

    // Normally contains the spot with the least amount of players
    public List<Position> leastPlayerSpot = new ArrayList<>();

    // Position that the bot will go back to in the case of Stand Still playstyle
    public Position standStillCoord = null;

    // Timer for when to pick up thrown equipment
    public SudoTimer thrownPickupTimer = new SudoTimer(25000, 75000);
    
    // Branches
    private Root root = new Root(this);
    public IsCrabShellNull isCrabShellNull = new IsCrabShellNull(this);
    public IsStandStillPlaystyle isStandStillPlaystyle = new IsStandStillPlaystyle(this);
    public AttackBranch attackBranch = new AttackBranch(this);
    public DoesAvailableSpotContainPlayer doesAvailableSpotContainPlayer = new DoesAvailableSpotContainPlayer(this);
    public IsAnotherOnSpot isAnotherOnSpot = new IsAnotherOnSpot(this);
    public IsActivePlaystyle isActivePlaystyle = new IsActivePlaystyle(this);
    public IsAttacking isAttacking = new IsAttacking(this);
    public HealBranch healBranch = new HealBranch(this);
    public IsInBankArea isInBankArea = new IsInBankArea(this);
    public IsBankOpen isBankOpen = new IsBankOpen(this);
    public IsRefillRequired isRefillRequired = new IsRefillRequired(this);
    public AreSkillsLow areSkillsLow = new AreSkillsLow(this);
    public IsThrownEquipmentAround isThrownEquipmentAround = new IsThrownEquipmentAround(this);
    public InventoryHasEquipment inventoryHasEquipment = new InventoryHasEquipment(this);
    public IsCrabSpotAvailable isCrabSpotAvailable = new IsCrabSpotAvailable(this);

    // Leaf Tasks
    public CrabNullLeaf crabNullLeaf = new CrabNullLeaf(this);
    public CurrentlyAttackingLeaf currentlyAttackingLeaf = new CurrentlyAttackingLeaf(this);
    public WorldHopLeaf worldHopLeaf = new WorldHopLeaf(this);
    public AttackCrabLeaf attackCrabLeaf = new AttackCrabLeaf(this);
    public HandleLeastPlayerSpotLeaf handleLeastPlayerSpotLeaf = new HandleLeastPlayerSpotLeaf(this);
    public HealLeaf healLeaf = new HealLeaf(this);
    public NoFoodLeaf noFoodLeaf = new NoFoodLeaf(this);
    public WaitingForRespawnLeaf waitingForRespawnLeaf = new WaitingForRespawnLeaf(this);
    public CheckShellFailSafeLeaf checkShellFailSafeLeaf = new CheckShellFailSafeLeaf(this);
    public TraversalLeaf traversalLeaf = new TraversalLeaf(this);
    public TriggerRefreshLeaf triggerRefreshLeaf = new TriggerRefreshLeaf(this);
    public SuccessfulRefreshLeaf successfulRefreshLeaf = new SuccessfulRefreshLeaf(this);
    public DisableBankRunLeaf disableBankRunLeaf = new DisableBankRunLeaf(this);
    public RefillInventoryLeaf refillInventoryLeaf = new RefillInventoryLeaf(this);
    public UsePotionLeaf usePotionLeaf = new UsePotionLeaf(this);
    public WieldThrownEquipmentLeaf wieldThrownEquipmentLeaf = new WieldThrownEquipmentLeaf(this);
    public InteractLeaf pickupThrownEquipmentLeaf = new InteractLeaf(this, nearestThrownEquipment, thrownEquipmentName, "Take");

    @Override
    public String getTitle() {
        return "SudoCrabber v1.0.0";
    }

    @Override
    public String getTitleHexColor(){
        return "#e8d64e";
    }

    @Override
    public void onStart(){
        super.onStart();
        
        checkSpecialTimer.start();
        thrownPickupTimer.start();

        Platform.runLater(() -> {
            try {
                Debug.log("Trying to load fxml");
                URL fxmlURL = new URL("https://s3.amazonaws.com/sudobot/fxml/SudoCrabber/CrabberGUI+v1.0.0.fxml");
                //Debug.log("FXML URL: " + fxmlURL.toString());

                FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
                fxmlLoader.setController(new CrabFXController(this));
                Parent root = fxmlLoader.load();
                Scene scene = new Scene(root);
                scene.getStylesheets().add("https://s3.amazonaws.com/sudobot/fxml/css/modena_dark.css");
                jfxPanel.setScene(scene);

                SwingUtilities.invokeLater(() -> {
                    jFrame.add(jfxPanel);
                    jFrame.pack();
                    jFrame.setLocationRelativeTo(null);
                    jFrame.setVisible(true);
                }); // go for it
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public BranchTask getRootTask() {
        return root;
    }

    @Override
    public void notify(ItemTableEvent itemTableEvent) {

    }

    @Override
    public boolean readyForLogoff(){
        if (Game.isLoggedIn()) {
            //Execution.delayUntil(() -> ApexPlayer.isTargetting(script.crabName), 2500, 7500);
            if (refreshArea.contains(player)) {
                setCurrentTask("Player safely in refresh area, attempting to log off.");
                return true;
            } else {
                setCurrentTask("Running to refresh area to break.");
                Movement.walkTo(refreshArea.get(Random.nextInt(0, refreshArea.size() - 1)));
            }
        }
        return false;
    }

    // Determines if the player's inventory is lacking potions or food
    public boolean isRefillInventoryRequired(){
        if(banking) {
            boolean foodResults = !Inventory.contains(i -> Arrays.asList(i.getActions()).contains("Eat"));
            Debug.log("Food Results: " + foodResults);

            if (potionType == Potion.SP_ATT_STR) {
                boolean attackPotionResults = !Inventory.contains(i -> i.getName().matches(Potion.SP_ATT.getPatternAsString()));
                boolean strengthPotionResults = !Inventory.contains(i -> i.getName().matches(Potion.SP_STR.getPatternAsString()));

                return (usingFood && foodResults) || (usingPotion && (attackPotionResults || strengthPotionResults));
            }
            else if (potionType == Potion.ATT_STR) {
                boolean attackPotionResults = !Inventory.contains(i -> i.getName().matches(Potion.ATT.getPatternAsString()));
                boolean strengthPotionResults = !Inventory.contains(i -> i.getName().matches(Potion.STR.getPatternAsString()));

                return (usingFood && foodResults) || (usingPotion && (attackPotionResults || strengthPotionResults));
            }else {
                boolean potionResults = !Inventory.contains(i -> i.getName().matches(potionType.getPatternAsString()));

                return (usingFood && foodResults) || (usingPotion && potionResults);
            }
        }
        return false;
    }

    // Attempts to determine if the player is in one of the crab spots
    public boolean isPlayerOnCrabSpot()
    {
        if (player != null)
        {
            if(playStyle != Playstyle.StandStill)
            {
                for (ArrayList<Position> spotList : spotLists)
                {
                    if (spotList.contains(player.getPosition()))
                        return true;
                }
            }else
                return standStillCoord.distance(player.getPosition()) < 1;
        }
        return false;
    }

    public Area getRandomSpotArea(){
        ArrayList<ArrayList<Position>> lists = new ArrayList<ArrayList<Position>>();
        if(isPlayerOnCrabSpot())
        {
            spotLists.forEach(i -> lists.add(i));
            lists.remove(getLocalCrabSpot());
        }

        return getNearestCrabSpot(lists);
    }

    private Area getNearestCrabSpot(ArrayList<ArrayList<Position>> lists) {
        Position coord = null;
        double distance = 500;

        for (ArrayList<Position> i : lists) {
            for (Position c : i) {
                if (player.distance(c) < distance) {
                    coord = c;
                    distance = player.distance(c);
                }
            }
        }

        if (coord == null)
            return crabArea;
        else
            return Area.rectangular(coord, coord);
    }

    public ArrayList<Position> getLocalCrabSpot(){
        for(int i = 0; i < spotLists.size(); i++){
            for(int j = 0; j < spotLists.get(i).size(); j++){
                if(player.getPosition() == spotLists.get(i).get(j))
                    return spotLists.get(i);
            }
        }
        return null;
    }
}
