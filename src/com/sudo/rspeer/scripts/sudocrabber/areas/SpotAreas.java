package com.sudo.rspeer.scripts.sudocrabber.areas;

import org.rspeer.runetek.api.movement.position.Position;

import java.util.ArrayList;

public class SpotAreas
{
    public static ArrayList<ArrayList<Position>> sandCrabShoreSpots = new ArrayList<ArrayList<Position>>(){{
        add(new ArrayList<Position>(){{
            add(new Position(1776, 3468, 0));
            add(new Position(1775, 3469, 0));
            add(new Position(1775, 3468, 0));
            add(new Position(1776, 3469, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1790, 3468, 0));
            add(new Position(1791, 3468, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1772, 3460, 0));
            add(new Position(1773, 3461, 0));
            add(new Position(1773, 3460, 0));
            add(new Position(1772, 3462, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1764, 3469, 0));
            add(new Position(1765, 3470, 0));
            add(new Position(1764, 3470, 0));
            add(new Position(1765, 3469, 0));
        }});
    }};

    public static ArrayList<ArrayList<Position>> sandCrabIslandSpots = new ArrayList<ArrayList<Position>>(){{
        add(new ArrayList<Position>(){{
            add(new Position(1764, 3445, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1758, 3439, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1751, 3425, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1749, 3412, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1768, 3409, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1779, 3428, 0));
            add(new Position(1777, 3429, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1780, 3438, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1780, 3407, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(1786, 3404, 0));
        }});
    }};

    public static ArrayList<ArrayList<Position>> rockCrabSpots = new ArrayList<ArrayList<Position>>(){{
        add(new ArrayList<Position>(){{
            add(new Position(2701, 3719, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(2705, 3726, 0));
            add(new Position(2704, 3726, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(2718, 3720, 0));
            add(new Position(2717, 3720, 0));
            add(new Position(2718, 3721, 0));
            add(new Position(2717, 3721, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(2718, 3720, 0));
            add(new Position(2719, 3720, 0));
            add(new Position(2718, 3719, 0));
            add(new Position(2719, 3719, 0));
        }});

        add(new ArrayList<Position>(){{
            add(new Position(2709, 3719, 0));
            add(new Position(2709, 3720, 0));
            add(new Position(2709, 3721, 0));
        }});
    }};
}
