package com.sudo.rspeer.scripts.sudocrabber.areas;

import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public class TraversalAreas {

    public final static Area ZEAH_BANK_AREA = Area.rectangular(new Position(1711, 3469, 0), new Position(1722, 3460, 0));

    public final static Area ZEAH_CRAB_ISLAND_GOTO_AREA = Area.polygonal(new Position(1774, 3461, 0), new Position(1773, 3457, 0), new Position(1776, 3454, 0), new Position(1780, 3453, 0), new Position(1785, 3454, 0), new Position(1787, 3457, 0), new Position(1786, 3460, 0), new Position(1782, 3462, 0), new Position(1778, 3462, 0));
    public final static Area ZEAH_CRAB_ISLAND_LEAVE_AREA = Area.polygonal(new Position(1780, 3421, 0), new Position(1777, 3420, 0), new Position(1777, 3416, 0), new Position(1777, 3414, 0), new Position(1780, 3413, 0), new Position(1783, 3413, 0), new Position(1784, 3416, 0), new Position(1783, 3419, 0));
}
