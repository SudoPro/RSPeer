package com.sudo.rspeer.scripts.sudocrabber.areas;

import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public class CrabAreas
{
    public final static Area ZEAH_SHORE_SANDCRAB_AREA =  Area.polygonal(new Position(1726, 3471, 0), new Position(1726, 3456, 0), new Position(1731, 3457, 0), new Position(1742, 3467, 0), new Position(1748, 3460, 0), new Position(1754, 3466, 0), new Position(1776, 3454, 0), new Position(1798, 3457, 0), new Position(1800, 3469, 0), new Position(1795, 3474, 0), new Position(1775, 3472, 0), new Position(1761, 3473, 0), new Position(1744, 3480, 0));
    public final static Area ZEAH_SHORE_SANDCRAB_REFRESH_AREA = Area.rectangular(new Position(1765, 3500, 0), new Position(1791, 3509, 0));

    public final static Area ZEAH_ISLAND_SANDCRAB_AREA = Area.polygonal(new Position(1775, 3446, 0), new Position(1771, 3449, 0), new Position(1765, 3450, 0), new Position(1757, 3442, 0), new Position(1752, 3437, 0), new Position(1750, 3430, 0), new Position(1747, 3425, 0), new Position(1746, 3422, 0), new Position(1748, 3419, 0), new Position(1744, 3411, 0), new Position(1746, 3405, 0), new Position(1753, 3404, 0), new Position(1759, 3408, 0), new Position(1767, 3404, 0), new Position(1771, 3405, 0), new Position(1775, 3408, 0), new Position(1778, 3404, 0), new Position(1783, 3401, 0), new Position(1788, 3400, 0), new Position(1796, 3404, 0), new Position(1799, 3409, 0), new Position(1796, 3417, 0), new Position(1794, 3412, 0), new Position(1790, 3411, 0), new Position(1788, 3414, 0), new Position(1786, 3412, 0), new Position(1781, 3415, 0), new Position(1784, 3428, 0), new Position(1783, 3436, 0), new Position(1779, 3446, 0), new Position(1778, 3443, 0), new Position(1774, 3431, 0), new Position(1772, 3427, 0), new Position(1767, 3428, 0), new Position(1764, 3432, 0), new Position(1765, 3439, 0), new Position(1769, 3444, 0));
    public final static Area ZEAH_ISLAND_SANDCRAB_REFRESH_AREA = Area.rectangular(new Position(1765, 3500, 0), new Position(1791, 3509, 0));

    public final static Area FREM_ROCKCRAB_AREA = Area.rectangular(new Position(2721, 3713, 0), new Position(2696, 3730, 0));
    public final static Area FREM_ROCKCRAB_REFRESH_AREA = Area.rectangular(new Position(2730, 3668, 0), new Position(2696, 3684, 0));
}
