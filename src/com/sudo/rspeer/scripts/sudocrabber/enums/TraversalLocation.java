package com.sudo.rspeer.scripts.sudocrabber.enums;

public enum TraversalLocation
{
    refreshArea,
    crabSpot,
    crabArea,
    crabSpotArea,
    bankArea,
}
