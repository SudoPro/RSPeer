package com.sudo.rspeer.scripts.sudocrabber.enums;

public enum BotLocation {
    ZeahShore,
    CrabIsland,
    FremShore
}
