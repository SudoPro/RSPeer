package com.sudo.rspeer.scripts.sudocrabber.enums;

import com.sudo.rspeer.api.util.Regex;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;

import java.util.regex.Pattern;

public enum Potion {
    SP_ATT{
        @Override
        public boolean areSkillsLow(){
            return Skills.getCurrentLevel(Skill.ATTACK) - Skills.getLevel(Skill.ATTACK) < Skills.getLevel(Skill.ATTACK) * 0.1;
        }
    },
    SP_STR{
        @Override
        public boolean areSkillsLow(){
            return Skills.getCurrentLevel(Skill.STRENGTH) - Skills.getLevel(Skill.STRENGTH) < Skills.getLevel(Skill.STRENGTH) * 0.1;
        }
    },
    SP_ATT_STR{
        @Override
        public boolean areSkillsLow(){
            return Skills.getCurrentLevel(Skill.ATTACK) - Skills.getLevel(Skill.ATTACK) < Skills.getLevel(Skill.ATTACK) * 0.1 || Skills.getCurrentLevel(Skill.STRENGTH) - Skills.getLevel(Skill.STRENGTH) < Skills.getLevel(Skill.STRENGTH) * 0.1;
        }
    },
    ZAM{
        @Override
        public boolean areSkillsLow(){
            return Skills.getCurrentLevel(Skill.ATTACK) - Skills.getLevel(Skill.ATTACK) < Skills.getLevel(Skill.ATTACK) * 0.1;
        }
    },
    RANGED{
        @Override
        public boolean areSkillsLow(){
            return Skills.getCurrentLevel(Skill.RANGED) - Skills.getLevel(Skill.RANGED) < Skills.getLevel(Skill.RANGED) * 0.1;
        }
    },
    SP_COMBAT{
        @Override
        public boolean areSkillsLow() {
            return Skills.getCurrentLevel(Skill.ATTACK) - Skills.getLevel(Skill.ATTACK) < Skills.getLevel(Skill.ATTACK) * 0.1 || Skills.getCurrentLevel(Skill.STRENGTH) - Skills.getLevel(Skill.STRENGTH) < Skills.getLevel(Skill.STRENGTH) * 0.1;
        }
    },
    ATT{
        @Override
        public boolean areSkillsLow(){
            return Skills.getCurrentLevel(Skill.ATTACK) - Skills.getLevel(Skill.ATTACK) < Skills.getLevel(Skill.ATTACK) * 0.1;
        }
    },
    STR{
        @Override
        public boolean areSkillsLow(){
            return Skills.getCurrentLevel(Skill.STRENGTH) - Skills.getLevel(Skill.STRENGTH) < Skills.getLevel(Skill.STRENGTH) * 0.1;
        }
    },
    ATT_STR{
        @Override
        public boolean areSkillsLow(){
            return Skills.getCurrentLevel(Skill.ATTACK) - Skills.getLevel(Skill.ATTACK) < Skills.getLevel(Skill.ATTACK) * 0.1 || Skills.getCurrentLevel(Skill.STRENGTH) - Skills.getLevel(Skill.STRENGTH) < Skills.getLevel(Skill.STRENGTH) * 0.1;
        }
    };

    private String[] potionList;
    private String[] fullPotion;
    private String potionName, patternAsString;
    private Pattern pattern;

    static {
        SP_ATT.potionList = new String[]{"Super attack(4)", "Super attack(3)", "Super attack(1)", "Super attack(1)"};
        SP_STR.potionList = new String[]{"Super strength(4)", "Super strength(3)", "Super strength(2)", "Super strength(1)"};
        SP_ATT_STR.potionList = new String[]{"Super attack(4)", "Super attack(3)", "Super attack(2)", "Super attack(1)", "Super strength(4)", "Super strength(3)", "Super strength(2)", "Super strength(1)"};
        ZAM.potionList = new String[]{"Zamorak brew(4)", "Zamorak brew(3)", "Zamorak brew(2)", "Zamorak brew(1)"};
        RANGED.potionList = new String[]{"Ranging potion(4)", "Ranging potion(3)", "Ranging potion(2)", "Ranging potion(1)"};
        SP_COMBAT.potionList = new String[]{"Super combat potion(4)", "Super combat potion(3)", "Super combat potion(2)", "Super combat potion(1)"};
        ATT.potionList = new String[]{"Attack potion(4)", "Attack potion(3)", "Attack potion(1)", "Attack potion(1)"};
        STR.potionList = new String[]{"Strength potion(4)", "Strength potion(3)", "Strength potion(2)", "Strength potion(1)"};
        ATT_STR.potionList = new String[]{"Attack potion(4)", "Attack potion(3)", "Attack potion(2)", "Attack potion(1)", "Strength potion(4)", "Strength potion(3)", "Strength potion(2)", "Strength potion(1)"};

        SP_ATT.fullPotion = new String[]{"Super attack(4)"};
        SP_STR.fullPotion = new String[]{"Super strength(4)"};
        SP_ATT_STR.fullPotion = new String[]{"Super attack(4)", "Super strength(4)"};
        ZAM.fullPotion = new String[]{"Zamorak brew(4)"};
        RANGED.fullPotion = new String[]{"Ranging potion(4)"};
        SP_COMBAT.fullPotion = new String[]{"Super combat potion(4)"};
        ATT.fullPotion = new String[]{"Attack potion(4)"};
        STR.fullPotion = new String[]{"Strength potion(4)"};
        ATT_STR.fullPotion = new String[]{"Attack potion(4)", "Strength potion(4)"};

        SP_ATT.potionName = "Super Attack";
        SP_STR.potionName = "Super Strength";
        SP_ATT_STR.potionName = "Super Att & Str";
        ZAM.potionName = "Zamorak brew";
        RANGED.potionName = "Ranging";
        SP_COMBAT.potionName = "Super Combat";
        SP_ATT.potionName = "Attack Potion";
        SP_STR.potionName = "Strength Potion";
        SP_ATT_STR.potionName = "Att & Str Potions";

        SP_ATT.pattern = Regex.getPatternForContainsString("attack");
        SP_STR.pattern = Regex.getPatternForContainsString("strength");
        SP_ATT_STR.pattern = Regex.getPatternForContainsAnyOf("attack", "strength");
        ZAM.pattern = Regex.getPatternForContainsString("Zamorak brew");
        RANGED.pattern = Regex.getPatternForContainsString("Ranging");
        SP_COMBAT.pattern = Regex.getPatternForContainsString("combat");
        ATT.pattern = Regex.getPatternForContainsString("Attack po");
        STR.pattern = Regex.getPatternForContainsString("Strength po");
        ATT_STR.pattern = Regex.getPatternForContainsAnyOf("attack po", "strength po");

        SP_ATT.patternAsString = Regex.getStringPatternForContainsString("attack", true);
        SP_STR.patternAsString = Regex.getStringPatternForContainsString("strength", true);
        SP_ATT_STR.patternAsString = Regex.getPatternStringForContainsAnyOf("attack", "strength");
        ZAM.patternAsString = Regex.getStringPatternForContainsString("Zamorak brew", true);
        RANGED.patternAsString = Regex.getStringPatternForContainsString("Ranging", true);
        SP_COMBAT.patternAsString = Regex.getStringPatternForContainsString("combat", true);
        ATT.patternAsString = Regex.getStringPatternForContainsString("Attack po", true);
        STR.patternAsString = Regex.getStringPatternForContainsString("Strength po", true);
        ATT_STR.patternAsString = Regex.getPatternStringForContainsAnyOf("attack po", "strength po");

    }

    public String[] getPotionList(){
        return potionList;
    }

    public String getPotionName(){
        return potionName;
    }

    public Pattern getPattern(){
        return pattern;
    }

    public abstract boolean areSkillsLow();

    public String getPatternAsString() { return patternAsString; }
}
