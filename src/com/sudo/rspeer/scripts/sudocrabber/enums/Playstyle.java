package com.sudo.rspeer.scripts.sudocrabber.enums;

public enum Playstyle
{
    Active,
    StandStill,
    SpotJump
}
