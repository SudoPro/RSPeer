package com.sudo.rspeer.api.antiban;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.api.util.SudoTimer;
import org.rspeer.script.events.WelcomeScreen;

public class BreakHandler {

    private SudoTimer breakTimer, botTimer;
    private SudoScript script;

    // minBreakDuration = 15 minutes
    // maxBreakDuration = 120 minutes
    // minDurationBetweenBreak = 60 minutes
    // maxDurationBetweenBreak = 360 minutes

    // Below is the above minutes represented in milliseconds (multiplied by  60,000)
    private int minBreakDuration = 900000, maxBreakDuration = 7200000, minDurationBetweenBreak = 3600000, maxDurationBetweenBreak = 21600000;

    public BreakHandler(SudoScript script) {
        this.script = script;
    }

    public void startTimer() {
        breakTimer = new SudoTimer(minBreakDuration, maxBreakDuration);
        botTimer = new SudoTimer(minDurationBetweenBreak, maxDurationBetweenBreak);
        botTimer.start();
    }

    public void execute() {
        if (script.isBreaking) {
            if (breakTimer.getRemainingTime() <= 0) {
                botTimer = new SudoTimer(minDurationBetweenBreak, maxDurationBetweenBreak);
                Debug.log("Breaking complete. Next break will occur once Runtime reaches: " + SudoTimer.convertSecondsToFullClockString(script.stopwatch.getElapsed().getSeconds() + breakTimer.getRemainingTimeInSeconds()));
                //GameEvents.Universal.LOBBY_HANDLER.enable();
                //GameEvents.Universal.LOGIN_HANDLER.enable();
                //GameEvents.Universal.INTERFACE_CLOSER.enable();
                script.addBlockingEvent(new WelcomeScreen(script));
                botTimer.start();
                script.isBreaking = false;
            }
        } else {
            if (botTimer.getRemainingTime() <= 0) {
                breakTimer = new SudoTimer(minBreakDuration, maxBreakDuration);
                //GameEvents.Universal.LOBBY_HANDLER.disable();
                //GameEvents.Universal.LOGIN_HANDLER.disable();
                //GameEvents.Universal.INTERFACE_CLOSER.disable();
                script.removeBlockingEvent(WelcomeScreen.class);
                Debug.log("Break has been triggered. Bot now breaking for " + breakTimer.getRemainingTimeInMinutes() + " minutes.");
                Debug.log("Breaking complete. Next break will occur once Runtime reaches: " + SudoTimer.convertSecondsToFullClockString(script.stopwatch.getElapsed().getSeconds() + breakTimer.getRemainingTimeInSeconds()));
                breakTimer.start();
                script.isBreaking = true;
            }
        }
    }

    public void setMaxBreakDuration(int max) {
        maxBreakDuration = max;
    }

    public void setMinBreakDuration(int min) {
        minBreakDuration = min;
    }

    public int getMaxBreakDuration() {
        return maxBreakDuration;
    }

    public int getMinBreakDuration() {
        return minBreakDuration;
    }

    public void setMaxDurationBetweenBreak(int max) {
        maxDurationBetweenBreak = max;
    }

    public void setMinDurationBetweenBreak(int min) {
        minDurationBetweenBreak = min;
    }

    public int getMaxDurationBetweenBreak() {
        return maxDurationBetweenBreak;
    }

    public int getMinDurationBetweenBreak() {
        return minDurationBetweenBreak;
    }

    public SudoTimer getBotTimer(){
        return botTimer;
    }

    public SudoTimer getBreakTimer(){
        return breakTimer;
    }
}