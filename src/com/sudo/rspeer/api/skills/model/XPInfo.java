package com.sudo.rspeer.api.skills.model;

import com.sudo.rspeer.api.util.SudoMath;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;

import java.util.ArrayList;
import java.util.List;

public class XPInfo {
    private List<String> list;

    public Skill skillType;
    public int startLvl;
    public int startXP;
    public int currentLvl;
    public int currentXP;

    public XPInfo(Skill skillType){
        this.skillType = skillType;
        startLvl = Skills.getLevel(skillType);
        startXP = Skills.getExperience(skillType);
        currentLvl = startLvl;
        currentXP = startXP;
    }

    public void update(){
        currentLvl = Skills.getLevel(skillType);
        currentXP = Skills.getExperience(skillType);
    }

    public int getLevelsGained(){
        return currentLvl - startLvl;
    }

    public int getGainedXP() {
        return currentXP - startXP;
    }

    public int getXPHour(StopWatch watch){
        return SudoMath.getItemPerHour(watch, getGainedXP());
    }

    public List<String> getDetails(StopWatch stopWatch) {
        list = new ArrayList<String>();
        if (currentXP > startXP) {
            list.add(skillType.toString() + " Level: " + Integer.toString(currentLvl) + " (" + Integer.toString(getLevelsGained()) + " Gained)");
            list.add(skillType.toString() + " XP Gained: " + Integer.toString(getGainedXP()) + " (" + Integer.toString(getXPHour(stopWatch)) + "/Hour)");
        }
        return list;
    }
}