package com.sudo.rspeer.api.util;

import java.util.regex.Pattern;

public class Regex {

    public static Pattern getPatternForContainsString(String string){
        return Pattern.compile("(?i).*" + string + ".*");
    }

    public static Pattern getPatternForContainsString(String string, boolean caseInsensitive){
        return getPatternForContainsString(caseInsensitive ? "(?i).*" + string + ".*" : ".*" + string + ".*");
    }

    public static String getStringPatternForContainsString(String string){
        return Pattern.compile("(?i).*" + string + ".*").toString();
    }

    public static String getStringPatternForContainsString(String string, boolean caseInsensitive){
        return getPatternForContainsString(caseInsensitive ? "(?i).*" + string + ".*" : ".*" + string + ".*").toString();
    }

    public static Pattern getPatternForContainsAnyOf(String... strings){
        return getPatternForContainsString(getPatternStringForContainsAnyOf(strings));
    }

    public static String getPatternStringForContainsAnyOf(String... strings){
        String value = "(?i).*(";
        if(strings.length > 1 ) {
            for (String string : strings) {
                value = value + string + "|";
            }
        } else {
            value = value + strings[0];
        }

        value = value + ")";
        return value;
    }
}
