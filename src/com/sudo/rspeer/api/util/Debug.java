package com.sudo.rspeer.api.util;

public class Debug {
    public static void log(String message){
        System.out.println(message);
    }

    public static void log(String playerName, String branchName, boolean validate){
        log(playerName + " -> " + branchName + ": " + validate);
    }
}
