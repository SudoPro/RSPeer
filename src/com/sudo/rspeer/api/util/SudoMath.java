package com.sudo.rspeer.api.util;

import org.rspeer.runetek.api.commons.StopWatch;

public class SudoMath {

    public static int getItemPerHour(StopWatch stopwatch, int itemCount){
        if(itemCount == 0){
            return 0;
        } else {
            double seconds = stopwatch.getElapsed().getSeconds();
            double hours = seconds / 3600.0;
            double itemPerHour = (double)itemCount / hours;
            return (int)(itemPerHour);
        }
    }

}
