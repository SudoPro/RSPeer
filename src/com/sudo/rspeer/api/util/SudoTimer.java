package com.sudo.rspeer.api.util;

import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.commons.math.Random;

public class SudoTimer {

    private final StopWatch stopWatch = StopWatch.start();
    private int minTime, maxTime;
    private int time;
    private boolean hasStarted = false;

    public SudoTimer(int min, int max) {
        minTime = min;
        maxTime = max;
        time = Random.nextInt(minTime, maxTime);
    }

    public long getRemainingTime() {
        return time - stopWatch.getElapsed().toMillis();
    }

    public long getRemainingTimeInSeconds() {
        return getRemainingTime() / 1000;
    }

    public long getRemainingTimeInMinutes() {
        return getRemainingTime() / 60000;
    }

    public boolean hasExpired() {
        return getRemainingTime() <= 0;
    }

    public void reset() {
        time = Random.nextInt(minTime, maxTime);
        stopWatch.reset();
    }

    public void restartSameTimer() {
        stopWatch.reset();
    }

    public void start() {
        hasStarted = true;
        stopWatch.start();
    }

    public boolean hasStarted(){
        return hasStarted;
    }

    public static String convertSecondsToFullClockString(long timeInSeconds) {
        long days = timeInSeconds / 86400,
                hours = (timeInSeconds % 86400) / 3600,
                minutes = ((timeInSeconds % 86400) % 3600) / 60,
                seconds = timeInSeconds % 60;

        if (timeInSeconds > 86400)
            return getClockFormat(days) + ":" + getClockFormat(hours) + ":" + getClockFormat(minutes) + ":" + getClockFormat(seconds);
        else
            return getClockFormat(hours) + ":" + getClockFormat(minutes) + ":" + getClockFormat(seconds);
    }

    private static String getClockFormat(long number){
        if(number < 10)
            return "0" + Long.toString(number);
        else return Long.toString(number);
    }
}