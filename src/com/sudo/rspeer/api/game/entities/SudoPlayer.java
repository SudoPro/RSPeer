package com.sudo.rspeer.api.game.entities;

import org.rspeer.runetek.adapter.scene.PathingEntity;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.scene.Players;

import java.util.Arrays;

public class SudoPlayer {
    public static boolean isTargetting(String... npcName){
        Player local = Players.getLocal();
        PathingEntity target;
        String targetName;
        return local != null && (target = local.getTarget()) != null && (targetName = target.getName()) != null && Arrays.stream(npcName).anyMatch(targetName::equals) && Interfaces.canContinue() == false;
    }
}
