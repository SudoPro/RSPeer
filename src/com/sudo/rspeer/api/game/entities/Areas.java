package com.sudo.rspeer.api.game.entities;

import org.rspeer.runetek.adapter.Positionable;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

import java.util.List;

public class Areas {

    public static boolean anyContainsPositionable(List<Area> areaList, Position pos){
        for(int i = 0; i < areaList.size(); i++){
            if(areaList.get(i).contains(pos))
                return true;
        }
        return false;
    }

    public static Position getRandomPosition(Area area){
        return area.getTiles().get(Random.nextInt(0, area.getTiles().size() - 1));
    }

    public static Area getAreaContaining(List<Area> areas, Positionable positionable){
        Area area = null;

        for(int i = 0; i < areas.size(); i++){
            if(areas.get(i).contains(positionable))
                area = areas.get(i);

            if(area != null)
                break;
        }

        return area;
    }

    public static Area radiusRectangle(Positionable positionable, int radius){
        return Area.rectangular(positionable.getX() - radius, positionable.getY() - radius, positionable.getX() + radius, positionable.getY() + radius);
    }

    public final static Area CATH_RS3_BANK_AREA = Area.rectangular(new Position(2797, 3442, 0), new Position(2794, 3437, 0));
    public final static Area CATH_OSRS_BANK_AREA = Area.rectangular(new Position(2812, 3442, 0), new Position(2807, 3438, 0));
    public final static Area CATH_FISH_AREA = Area.rectangular(new Position(2834, 3433, 0), new Position(2864, 3423, 0));
    public final static Area CATH_OSRS_RANGE_AREA = Area.rectangular(new Position(2815, 3444, 0), new Position(2818, 3438, 0));
    public final static Area CATH_RS3_RANGE_AREA = Area.rectangular(new Position(2795, 3450, 0), new Position(2798, 3453, 0));
    public final static Area CATH_WILL_TREE_AREA = Area.polygonal(new Position[]{new Position(2786, 3428, 0), new Position(2787, 3430, 0), new Position(2784, 3432, 0), new Position(2778, 3432, 0), new Position(2776, 3428, 0), new Position(2780, 3426, 0), new Position(2783, 3425, 0)});
    public final static Area CATH_WILL_TREE_ACTION_AREA = Area.polygonal(new Position[]{new Position(2770, 3431, 0), new Position(2766, 3428, 0), new Position(2766, 3423, 0), new Position(2773, 3422, 0), new Position(2779, 3421, 0), new Position(2787, 3422, 0), new Position(2790, 3427, 0), new Position(2791, 3433, 0), new Position(2785, 3435, 0)});
    public final static Area CATH_OSRS_OAK_TREE_AREA = Area.polygonal(new Position[]{new Position(2804, 3443, 0), new Position(2804, 3450, 0), new Position(2796, 3453, 0), new Position(2787, 3457, 0), new Position(2776, 3458, 0), new Position(2768, 3456, 0), new Position(2767, 3449, 0), new Position(2767, 3442, 0), new Position(2771, 3439, 0), new Position(2778, 3437, 0), new Position(2783, 3431, 0), new Position(2793, 3430, 0)});
    public final static Area CATH_RS3_OAK_TREE_AREA = Area.rectangular(new Position(2767, 3425, 0), new Position(2790, 3446, 0));
    public final static Area CATH_YEW_TREE_AREA = Area.polygonal(new Position[]{new Position(2760, 3429, 0), new Position(2767, 3431, 0), new Position(2768, 3434, 0), new Position(2757, 3434, 0), new Position(2754, 3431, 0)});
    public final static Area CATH_YEW_TREE_ACTION_AREA = Area.polygonal(new Position[]{new Position(2748, 3436, 0), new Position(2748, 3432, 0), new Position(2750, 3427, 0), new Position(2753, 3424, 0), new Position(2765, 3424, 0), new Position(2772, 3428, 0), new Position(2776, 3438, 0), new Position(2772, 3443, 0), new Position(2767, 3440, 0), new Position(2764, 3437, 0)});
    public final static Area CATH_EVER_TREE_AREA = Area.polygonal(new Position[]{new Position(2789, 3474, 0), new Position(2797, 3470, 0), new Position(2806, 3470, 0), new Position(2810, 3474, 0), new Position(2811, 3479, 0), new Position(2804, 3483, 0), new Position(2805, 3487, 0), new Position(2802, 3489, 0), new Position(2796, 3487, 0), new Position(2790, 3492, 0), new Position(2784, 3493, 0), new Position(2776, 3492, 0), new Position(2777, 3479, 0), new Position(2783, 3474, 0)});

    public final static Area DRAY_BANK_AREA = Area.rectangular(new Position(3092, 3240, 0), new Position(3097, 3246, 0));
    public final static Area DRAY_FISH_AREA = Area.rectangular(new Position(3083, 3236, 0), new Position(3091, 3222, 0));
    public final static Area DRAY_WILL_TREE_AREA = Area.polygonal(new Position(3079, 3238, 0), new Position(3082, 3239, 0), new Position(3085, 3239, 0), new Position(3090, 3237, 0), new Position(3092, 3235, 0), new Position(3092, 3231, 0), new Position(3092, 3226, 0), new Position(3089, 3223, 0), new Position(3084, 3224, 0), new Position(3080, 3231, 0));
    public final static Area DRAY_WILL_TREE_ACTION_AREA = Area.polygonal(new Position[]{new Position(3085, 3240, 0), new Position(3082, 3242, 0), new Position(3078, 3240, 0), new Position(3081, 3232, 0), new Position(3085, 3223, 0), new Position(3090, 3224, 0), new Position(3092, 3227, 0), new Position(3089, 3237, 0)});
    public final static Area DRAY_NORM_N_TREE_AREA = Area.rectangular(new Position(3074, 3275, 0), new Position(3088, 3262, 0));
    public final static Area DRAY_NORM_SE_TREE_AREA = Area.polygonal(new Position(3098, 3254, 0), new Position(3098, 3232, 0), new Position(3111, 3212, 0), new Position(3131, 3209, 0), new Position(3146, 3219, 0), new Position(3155, 3263, 0), new Position(3117, 3264, 0), new Position(3101, 3256, 0));

    public final static Area LUMB_BANK_AREA = Area.rectangular(new Position(3211, 3259, 0), new Position(3214, 3254, 0));
    public final static Area LUMB_FISH_AREA = Area.rectangular(new Position(3242, 3260, 0), new Position(3239, 3238, 0));
    public final static Area LUMB_FISH_SPOT_AREA = Area.rectangular(new Position(3244, 3262, 0), new Position(3237, 3236, 0));

    public final static Area FALA_E_BANK_AREA = Area.rectangular(new Position(3009, 3358, 0), new Position(3017, 3355, 0));
    public final static Area FALA_YEW_SE_TREE_AREA = Area.rectangular(new Position(3022, 3358, 0), new Position(3015, 3320, 0));
    public final static Area FALA_YEW_SW_TREE_AREA = Area.polygonal(new Position[]{new Position(2994, 3309, 0), new Position(2999, 3309, 0), new Position(3001, 3313, 0), new Position(3001, 3320, 0), new Position(2994, 3314, 0)});
    public final static Area FALA_NORM_OAK_TREE_AREA = Area.polygonal(new Position(3005, 3321, 0), new Position(3001, 3321, 0), new Position(2995, 3314, 0), new Position(2987, 3306, 0), new Position(2983, 3305, 0), new Position(2977, 3298, 0), new Position(2973, 3289, 0), new Position(2977, 3282, 0), new Position(2985, 3279, 0), new Position(2993, 3284, 0), new Position(2999, 3281, 0), new Position(3012, 3289, 0), new Position(3012, 3311, 0), new Position(3020, 3312, 0), new Position(3024, 3315, 0), new Position(3042, 3314, 0), new Position(3043, 3326, 0), new Position(3025, 3326, 0), new Position(3023, 3323, 0), new Position(3016, 3324, 0), new Position(3011, 3320, 0));

    public final static Area VARROCK_E_BANK_AREA = Area.rectangular(new Position(3250, 3423, 0), new Position(3247, 3420, 0));

    public final static Area ARMIES_BANK_AREA = Area.rectangular(new Position(2400, 2843, 0), new Position(2405, 2840, 0));
    public final static Area ARMIES_FISH_AREA = Area.polygonal(new Position(2431, 2894, 0), new Position(2435, 2889, 0), new Position(2442, 2890, 0), new Position(2446, 2899, 0), new Position(2451, 2904, 0), new Position(2451, 2912, 0), new Position(2460, 2918, 0), new Position(2469, 2926, 0), new Position(2473, 2938, 0), new Position(2466, 2942, 0), new Position(2453, 2935, 0), new Position(2440, 2922, 0), new Position(2432, 2907, 0));

    public final static Area GUILD_BANK_RS3_AREA = Area.rectangular(new Position(2584, 3420, 0), new Position(2588, 3424, 0));
    public final static Area GUILD_BANK_OSRS_AREA = Area.polygonal(new Position(2583, 3422, 0), new Position(2583, 3417, 0), new Position(2586, 3417, 0), new Position(2587, 3416, 0), new Position(2587, 3414, 0), new Position(2589, 3414, 0), new Position(2589, 3421, 0), new Position(2588, 3422, 0));
    public final static Area GUILD_DOCK1_FISH_AREA = Area.rectangular(new Position(2597, 3419, 0), new Position(2605, 3426, 0));
    public final static Area GUILD_DOCK1_FISH_SPOT_AREA = Area.rectangular(new Position(2596, 3418, 0), new Position(2606, 3427, 0));
    public final static Area GUILD_DOCK2_FISH_AREA = Area.rectangular(new Position(2602, 3406, 0), new Position(2612, 3417, 0));
    public final static Area GUILD_DOCK2_FISH_SPOT_AREA = Area.rectangular(new Position(2601, 3405, 0), new Position(2613, 3418, 0));

    public final static Area EDGE_BANK_AREA = Area.rectangular(new Position(3098, 3489, 0), new Position(3091, 3498, 0));
    public final static Area BARB_FISH_AREA = Area.polygonal(new Position[]{new Position(3094, 3422, 0), new Position(3094, 3426, 0), new Position(3099, 3426, 0), new Position(3100, 3427, 0), new Position(3100, 3429, 0), new Position(3099, 3430, 0), new Position(3099, 3434, 0), new Position(3097, 3438, 0), new Position(3101, 3441, 0), new Position(3105, 3441, 0), new Position(3110, 3438, 0), new Position(3112, 3434, 0), new Position(3111, 3428, 0), new Position(3106, 3422, 0)});
    public final static Area EDGE_YEW_TREE_AREA = Area.rectangular(new Position(3089, 3468, 0), new Position(3085, 3482, 0));

    public final static Area GE_RS3_BANK_AREA = Area.rectangular(new Position(3162, 3481, 0), new Position(3174, 3501, 0));
    public final static Area GE_OSRS_BANK_AREA = Area.rectangular(new Position(3168, 3493, 0), new Position(3161, 3486, 0));
    public final static Area GE_YEW_TREE_AREA = Area.polygonal(new Position[]{new Position(3203, 3506, 0), new Position(3200, 3505, 0), new Position(3202, 3501, 0), new Position(3205, 3500, 0), new Position(3212, 3499, 0), new Position(3225, 3499, 0), new Position(3225, 3506, 0)});

    public final static Area SEER_BANK_AREA = Area.polygonal(new Position[]{new Position(2724, 3488, 0), new Position(2724, 3491, 0), new Position(2721, 3491, 0), new Position(2721, 3494, 0), new Position(2730, 3494, 0), new Position(2730, 3490, 0), new Position(2727, 3491, 0), new Position(2728, 3488, 0)});
    public final static Area SEER_WILL_TREE_AREA = Area.polygonal(new Position[]{new Position(2704, 3515, 0), new Position(2699, 3510, 0), new Position(2700, 3502, 0), new Position(2708, 3504, 0), new Position(2715, 3504, 0), new Position(2719, 3502, 0), new Position(2722, 3505, 0), new Position(2718, 3510, 0), new Position(2714, 3514, 0), new Position(2709, 3518, 0), new Position(2706, 3517, 0)});
    public final static Area SEER_MAPLE_TREE_AREA = Area.polygonal(new Position[]{new Position(2717, 3498, 0), new Position(2717, 3505, 0), new Position(2728, 3505, 0), new Position(2734, 3505, 0), new Position(2738, 3497, 0), new Position(2734, 3490, 0), new Position(2731, 3490, 0), new Position(2731, 3498, 0)});
    public final static Area SEER_YEW_TREE_AREA = Area.polygonal(new Position[]{new Position(2718, 3457, 0), new Position(2719, 3462, 0), new Position(2713, 3468, 0), new Position(2704, 3467, 0), new Position(2705, 3456, 0)});
    public final static Area SEER_MAGIC_SW_TREE_AREA = Area.polygonal(new Position[]{new Position(2704, 3423, 0), new Position(2703, 3431, 0), new Position(2691, 3433, 0), new Position(2686, 3429, 0), new Position(2687, 3423, 0), new Position(2691, 3419, 0), new Position(2702, 3420, 0)});
    public final static Area SEER_MAGIC_S_TREE_AREA = Area.polygonal(new Position[]{new Position(2701, 3400, 0), new Position(2698, 3397, 0), new Position(2698, 3396, 0), new Position(2700, 3394, 0), new Position(2704, 3394, 0), new Position(2706, 3396, 0), new Position(2706, 3397, 0), new Position(2703, 3400, 0)});
    public final static Area SEER_MAGIC_S_TREE_ACTION_AREA = Area.polygonal(new Position[]{new Position(2707, 3401, 0), new Position(2698, 3401, 0), new Position(2698, 3393, 0), new Position(2707, 3394, 0)});

    public final static Area ROGUE_RS3_BANK_AREA = Area.rectangular(new Position(3035, 4951, 0), new Position(3023, 4964, 0));
    public final static Area ROGUE_RS3_FIRE_AREA = Area.rectangular(new Position(3027, 4961, 0), new Position(3031, 4957, 0));
    public final static Area ROGUE_OSRS_BANK_AREA = Area.rectangular(new Position(3048, 4979, 1), new Position(3037, 4955, 1));
    public final static Area ROGUE_OSRS_FIRE_AREA = Area.rectangular(new Position(3041, 4971, 1), new Position(3045, 4975, 1));

    public final static Area ALKHARID_RS3_BANK_AREA = Area.rectangular(new Position(3268, 3168, 0), new Position(3272, 3164, 0));
    public final static Area ALKHARID_OSRS_BANK_AREA = Area.rectangular(new Position(3269, 3168, 0), new Position(3272, 3162, 0));
    public final static Area ALKHARID_RS3_RANGE_AREA = Area.rectangular(new Position(3269, 3184, 0), new Position(3272, 3181, 0));
    public final static Area ALKHARID_OSRS_RANGE_AREA = Area.rectangular(new Position(3271, 3182, 0), new Position(3275, 3179, 0));
    public final static Area ALKHARID_RS3_FISH_AREA = Area.polygonal(new Position(3240, 3151, 0), new Position(3243, 3148, 0), new Position(3249, 3151, 0), new Position(3251, 3164, 0), new Position(3257, 3152, 0), new Position(3265, 3155, 0), new Position(3264, 3177, 0), new Position(3239, 3175, 0));
    public final static Area ALKHARID_OSRS_FISH_AREA = Area.polygonal(new Position(3257, 3151, 0), new Position(3274, 3151, 0), new Position(3282, 3141, 0), new Position(3284, 3134, 0), new Position(3280, 3133, 0), new Position(3267, 3140, 0));

    public final static Area PISCATORIS_BANK_AREA = Area.rectangular(new Position(2327, 3693, 0), new Position(2332, 3686, 0));
    public final static Area PISCATORIS_FISH_AREA = Area.rectangular(new Position(2324, 3696, 0), new Position(2353, 3706, 0));

    public final static Area SHILO_BANK_AREA = Area.rectangular(new Position(2849, 2951, 0), new Position(2855, 2957, 0));
    public final static Area SHILO_FISH_AREA = Area.polygonal(new Position(2867, 2971, 0), new Position(2875, 2977, 0), new Position(2872, 2980, 0), new Position(2856, 2983, 0), new Position(2841, 2981, 0), new Position(2820, 2977, 0), new Position(2819, 2967, 0), new Position(2821, 2964, 0), new Position(2851, 2966, 0), new Position(2853, 2969, 0), new Position(2868, 2969, 0));

    public final static Area ZEAH_LAND_END_BANK_AREA = Area.rectangular(new Position(1512, 3419, 0), new Position(1509, 3423, 0));
    public final static Area ZEAH_LAND_END_MAGIC_TREE_AREA = Area.rectangular(new Position(1605, 3448, 0), new Position(1618, 3437, 0));

    public final static Area HOSIDIUS_BANK_AREA = Area.rectangular(new Position(1652, 3614, 0), new Position(1660, 3604, 0));
    public final static Area HOSIDIUS_OSRS_RANGE_AREA = HOSIDIUS_BANK_AREA;

    public final static Area MENAPHOS_PORT_DEPOSITBOX_AREA = Area.rectangular(new Position(3216, 2624, 0), new Position(3218, 2621, 0));
    public final static Area MENAPHOS_PORT_FISH_AREA = Area.rectangular(new Position(3209, 2632, 0), new Position(3215, 2624, 0));
    public final static Area MENAPHOS_VIP_BANK_AREA = Area.rectangular(new Position(3180, 2740, 0), new Position(3184, 2744, 0));
    public final static Area MENAPHOS_VIP_FISH_AREA = Area.rectangular(new Position(3182, 2745, 0), new Position(3190, 2755, 0));
    public final static Area MENAPHOS_VIP_TREE_AREA = Area.rectangular(new Position(3179, 2744, 0), new Position(3194, 2755, 0));

    public final static Area DEEP_SEA_SWARM_DEPOSITBOX_AREA = Area.rectangular(new Position(2095, 7093, 0), new Position(2101, 7088, 0));
    public final static Area DEEP_SEA_SWARM_FISH_AREA = Area.rectangular(new Position(2091, 7084, 0), new Position(2101,7078, 0));

    public final static Area COOK_GUILD_RANGE_AREA = Area.rectangular(new Position(3144, 3454, 0), new Position(3147, 3452, 0));
    public final static Area COOK_GUILD_BANK_AREA = Area.rectangular(new Position(3147, 3454, 0), new Position(3149,3448, 0));
    public final static Area COOK_GUILD_LOBBY_AREA = Area.polygonal(new Position(3144, 3453, 0), new Position(3144, 3450, 0), new Position(3147, 3450, 0), new Position(3147, 3446, 0), new Position(3145, 3444, 0), new Position(3142, 3444, 0), new Position(3138, 3448, 0), new Position(3138, 3451, 0), new Position(3140, 3453, 0));

    public final static Area WOOD_GUILD_BANK_AREA = Area.rectangular(new Position(1589, 3481, 0), new Position(1593, 3474, 0));
    public final static Area WOOD_GUILD_MAGIC_TREE_AREA = Area.rectangular(new Position(1579, 3494, 0), new Position(1582, 3480, 0));
    public final static Area WOOD_GUILD_YEW_TREE_AREA = Area.polygonal(new Position(1584, 3478, 0), new Position(1588, 3478, 0), new Position(1598, 3483, 0), new Position(1598, 3497, 0), new Position(1589, 3503, 0), new Position(1584, 3503, 0), new Position(1580, 3498, 0));
    public final static Area WOOD_GUILD_MAPLE_TREE_AREA = Area.rectangular(new Position(1608, 3498, 0), new Position(1627, 3487, 0));
    public final static Area WOOD_GUILD_WILLOW_TREE_AREA = Area.rectangular(new Position(1626, 3503, 0), new Position(1644, 3494, 0));
    public final static Area WOOD_GUILD_OAK_TREE_AREA = Area.rectangular(new Position(1615, 3515, 0), new Position(1629, 3507, 0));
    public final static Area WOOD_GUILD_NORMAL_TREE_AREA = Area.rectangular(new Position(1626, 3516, 0), new Position(1653, 3506, 0));
    public final static Area WOOD_GUILD_REDWOOD_LEVEL1_AREA = Area.rectangular(new Position(1567, 3496, 1), new Position(1574, 3479, 1));
    public final static Area WOOD_GUILD_REDWOOD_LEVEL2_AREA = Area.rectangular(new Position(1567, 3496, 2), new Position(1574, 3479, 2));
    public final static Area WOOD_GUILD_REDWOOD_LADDER_BASE_AREA  = Area.rectangular(new Position(1574, 3457, 0), new Position(1576, 3482, 0));
    public final static Area WOOD_GUILD_REDWOOD_LADDER_LEVEL1_AREA  = Area.rectangular(new Position(1568, 3493, 1), new Position(1575, 3472, 1));
    public final static Area WOOD_GUILD_REDWOOD_LADDER_LEVEL2_AREA  = Area.rectangular(new Position(1568, 3492, 2), new Position(1573, 3473, 2));

    public final static Area BARB_ASSAULT_BANK_AREA = Area.rectangular(new Position(2529, 3576, 0), new Position(2538, 3567, 0));
    public final static Area BAXTORIAN_FISH_AREA = Area.rectangular(new Position(2497, 3520, 0), new Position(2510, 3493, 0));

    public final static Area CITY_OF_VARROCK_AREA = Area.polygonal(
            new Position(3142, 3516, 0),
            new Position(3138, 3512, 0),
            new Position(3139, 3494, 0),
            new Position(3142, 3492, 0),
            new Position(3142, 3484, 0),
            new Position(3139, 3481, 0),
            new Position(3139, 3473, 0),
            new Position(3144, 3468, 0),
            new Position(3145, 3464, 0),
            new Position(3174, 3446, 0),
            new Position(3173, 3425, 0),
            new Position(3174, 3425, 0),
            new Position(3174, 3399, 0),
            new Position(3181, 3398, 0),
            new Position(3182, 3382, 0),
            new Position(3208, 3382, 0),
            new Position(3208, 3374, 0),
            new Position(3214, 3374, 0),
            new Position(3214, 3382, 0),
            new Position(3241, 3382, 0),
            new Position(3241, 3386, 0),
            new Position(3253, 3386, 0),
            new Position(3253, 3380, 0),
            new Position(3263, 3380, 0),
            new Position(3264, 3380, 0),
            new Position(3265, 3376, 0),
            new Position(3273, 3376, 0),
            new Position(3285, 3377, 0),
            new Position(3288, 3380, 0),
            new Position(3288, 3382, 0),
            new Position(3287, 3383, 0),
            new Position(3287, 3388, 0),
            new Position(3283, 3393, 0),
            new Position(3283, 3403, 0),
            new Position(3280, 3406, 0),
            new Position(3280, 3407, 0),
            new Position(3275, 3406, 0),
            new Position(3272, 3409, 0),
            new Position(3272, 3425, 0),
            new Position(3277, 3425, 0),
            new Position(3276, 3432, 0),
            new Position(3271, 3432, 0),
            new Position(3271, 3435, 0),
            new Position(3269, 3436, 0),
            new Position(3269, 3462, 0),
            new Position(3261, 3471, 0),
            new Position(3261, 3491, 0),
            new Position(3252, 3491, 0),
            new Position(3250, 3494, 0),
            new Position(3249, 3500, 0),
            new Position(3235, 3499, 0),
            new Position(3228, 3506, 0),
            new Position(3201, 3506, 0),
            new Position(3188, 3516, 0),
            new Position(3171, 3516, 0),
            new Position(3167, 3513, 0),
            new Position(3160, 3513, 0),
            new Position(3157, 3516, 0)
    );
}
