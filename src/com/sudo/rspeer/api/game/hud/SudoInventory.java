package com.sudo.rspeer.api.game.hud;

import com.sudo.rspeer.api.util.Regex;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SudoInventory {

    public static Item[] getItemsWithNameContains(String name){
        return Inventory.getItems(i -> i.getName().matches(Regex.getStringPatternForContainsString(name, true)));
    }

    public static String[] getItemNamesWithNameContains(String name){
        List<String> names = new ArrayList<>();
        for (Item item : getItemsWithNameContains(name)) {
            names.add(item.getName());
        }
        return names.toArray(new String[0]);
    }

    public static boolean containsAnyExcept(String... names){
        return Inventory.getItems(i -> !Arrays.asList(names).contains(i.getName())).length > 0;
    }

    public static void dropItem(Item item){
        if (item.interact("Drop")) {
            Time.sleep(35, 60);
        }
    }
}
