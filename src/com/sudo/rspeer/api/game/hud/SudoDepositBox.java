package com.sudo.rspeer.api.game.hud;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.component.DepositBox;
import org.rspeer.runetek.api.component.tab.Inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SudoDepositBox {

    public static void depositAllExcept(String... itemName){
        List<String> nameList = Arrays.asList(itemName),
                namesToDeposit = new ArrayList<>();
        Item[] items = Inventory.getItems();

        for(int i = 0; i < items.length; i++){
            if(!nameList.contains(items[i].getName()) && !namesToDeposit.contains(items[i].getName()))
                namesToDeposit.add(items[i].getName());
        }

        DepositBox.depositAll(namesToDeposit.toArray(new String[0]));
    }

}
