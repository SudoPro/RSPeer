package com.sudo.rspeer.base;

import com.sudo.api.Web;
import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.base.ui.awt.SudoGraphics;
import com.sudo.rspeer.api.antiban.BreakHandler;
import com.sudo.rspeer.api.skills.model.XPInfo;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.api.util.SudoTimer;
import com.sudo.rspeer.common.leafs.*;
import javafx.embed.swing.JFXPanel;
import org.rspeer.networking.api.RsPeerApi;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.ItemTableListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.ItemTableEvent;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.runetek.providers.RSItemDefinition;
import org.rspeer.script.Script;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

public abstract class SudoScript extends Script implements RenderListener, ItemTableListener {
    public Player player;
    public boolean firstLogin = true, waitingOnGUI = true, isDropping = false, breakingEnabled = false, isBreaking = false, runEnabled = true, useSpecial = true;
    public Boolean hasBeenValidated = null;

    public BreakHandler breakHandler = new BreakHandler(this);
    public int grossIncome = 0, stopAfterMinutes = Integer.MAX_VALUE;

    public final StopWatch stopwatch = StopWatch.start();
    public SudoTimer checkSpecialTimer = new SudoTimer(45000, 210000);

    private String currentTask;
    private String validateURL = "https://bo8zxgjc47.execute-api.us-east-1.amazonaws.com/default/ValidateRSPeerUserDB?value=";

    protected JFXPanel jfxPanel = new JFXPanel();
    protected JFrame jFrame = new JFrame();

    public HashMap<Skill, XPInfo> xpInfoMap = new HashMap<>();
    public HashMap<String, String> displayInfoMap = new HashMap<>();

    // Used to store ID's and their corresponding GE sell price
    private HashMap<Integer, Integer> acquiredIDs = new HashMap<Integer, Integer>(){{
        put(383, 997); // Raw shark
    }};

    // Branches
    private BranchTask rootTask;

    // Leafs
    public PowerDropLeaf powerDropLeaf;
    public PlayerNullLeaf playerNullLeaf = new PlayerNullLeaf(this);
    public WaitingOnGuiLeaf waitingOnGuiLeaf = new WaitingOnGuiLeaf(this);
    public BreakHandlerLeaf breakHandlerLeaf = new BreakHandlerLeaf(this);
    public UseSpecialAttackLeaf useSpecialAttackLeaf = new UseSpecialAttackLeaf(this);
    public OpenBankLeaf openBankLeaf = new OpenBankLeaf(this);
    public CloseBankLeaf closeBankLeaf = new CloseBankLeaf(this);
    public CloseDepositBoxLeaf closeDepositBoxLeaf = new CloseDepositBoxLeaf(this);
    public LogoutOfGameLeaf logoutOfGameLeaf = new LogoutOfGameLeaf(this);
    public NotifyLeaf unauthorizedSessionLeaf = new NotifyLeaf(this, "You do not have authorization to run this script."){
        @Override
        public void successAction(){
            setStopping(true);
        }
    };

    @Override
    public void onStart(){

    }

    @Override
    public int loop() {
        player = Players.getLocal();

        if (rootTask != null) {
            TreeTask.Traverse(rootTask);
        } else {
            rootTask = getRootTask();
        }
        return 100;
    }

    @Override
    public void onStop() {
        jFrame.dispatchEvent(new WindowEvent(jFrame, WindowEvent.WINDOW_CLOSING));
    }

    public void onItemAdded(ItemTableEvent event){
        RSItemDefinition definition = event.getDefinition();
        int ID;

        if (definition != null) {
            ID = definition.isNoted() ? definition.getUnnotedId() : definition.getId();
            if (!acquiredIDs.containsKey(ID)) {
                try {
                     //acquiredIDs.put(ID, OSBuddyExchange.getGuidePrice(ID).getSelling());
                } catch (Exception e) {
                    acquiredIDs.put(ID, 0);
                }
            }
            for (int i = 0; i < event.getStackSize() - event.getOldStackSize(); i++)
                if(acquiredIDs.get(ID) != null)
                    grossIncome += acquiredIDs.get(ID);
        }
    }

    public void onItemRemoved(ItemTableEvent event){
        RSItemDefinition definition = event.getDefinition();
        int ID;

        if (definition != null) {
            ID = definition.isNoted() ? definition.getUnnotedId() : definition.getId();
            if (!acquiredIDs.containsKey(ID)) {
                try {
                    //acquiredIDs.put(ID, OSBuddyExchange.getGuidePrice(ID).getSelling());
                } catch (Exception e) {
                    acquiredIDs.put(ID, 0);
                }
            }
            for (int i = 0; i < event.getStackSize() - event.getOldStackSize(); i++)
                if(acquiredIDs.get(ID) != null)
                    grossIncome -= acquiredIDs.get(ID);
        }
    }

    public boolean authorizedSession() {
        boolean value;
        try {
            value = Web.getHTML(validateURL + RsPeerApi.getCurrentUserName()).equals("true");
            if(value){
                Debug.log("Session has been successfully authenticated for RSPeer User: " + RsPeerApi.getCurrentUserName());
            } else {
                value = Game.isLoggedIn() && Web.getHTML(validateURL + Players.getLocal().getName()).equals("true");
                if(value) {
                    Debug.log("Session has been successfully authenticated for RS User: " + Players.getLocal().getName());
                }
            }
        } catch (Exception e) {
            Debug.log("Error validating for user: " + RsPeerApi.getCurrentUserName());
            value = false;
        }

        return value;
    }

    private Font smallFont = new Font("Ariel", Font.BOLD, 10),
            header2Font = new Font("Ariel", Font.BOLD, 12),
            headerFont = new Font("Ariel", Font.BOLD, 16);

    @Override
    public void notify(RenderEvent renderEvent) {
        int xAnchor = Game.isLoggedIn() ? 515 : 4;
        int statsGuiWidth = 230, statsGuiHeight = 40;
        int startX = xAnchor - statsGuiWidth, startY = 4;

        if(xpInfoMap != null && displayInfoMap != null){
            for(Skill skill : xpInfoMap.keySet()){
                if(xpInfoMap.get(skill).getGainedXP() > 0)
                    statsGuiHeight += 32;
            }

            for(String string : displayInfoMap.keySet()){
                statsGuiHeight += 16;
            }
        }

        Graphics g = renderEvent.getSource();
        g.setColor(new Color(60, 60, 60, 120));
        g.fillRect(startX, startY, statsGuiWidth, statsGuiHeight);
        g.setColor(Color.decode("#d5d5d5"));
        g.drawRect(startX, startY, statsGuiWidth, statsGuiHeight);
        g.drawLine(startX, startY + 20, startX + statsGuiWidth, startY + 20);

        g.setColor(Color.decode("#333333"));
        g.setFont(headerFont);
        SudoGraphics.drawHeaderCenterAlignment(g, getTitle(), statsGuiWidth, startX, startY + 16);
        g.setColor(Color.decode(getTitleHexColor()));
        SudoGraphics.drawStringCenterAlignment(g, getTitle(), statsGuiWidth, startX, startY + 16);

        g.setColor(Color.decode("#d5d5d5"));
        g.setFont(smallFont);


        int y = startY + 35;
        if(xpInfoMap != null) {
            for (Skill skill : xpInfoMap.keySet()) {
                if(xpInfoMap.get(skill).getGainedXP() > 0) {
                    SudoGraphics.drawTwoStringsCenterAlignment(g, skill.toString() + " Level: ",
                            Integer.toString(Skills.getLevel(skill)) + " (" + Integer.toString(xpInfoMap.get(skill).getLevelsGained()) + " Gained)", statsGuiWidth, startX, y);
                    y += 16;

                    SudoGraphics.drawTwoStringsCenterAlignment(g, skill.toString() + " XP: ", NumberFormat.getNumberInstance(Locale.US).format(xpInfoMap.get(skill).getGainedXP()) + " (" + NumberFormat.getNumberInstance(Locale.US).format(xpInfoMap.get(skill).getXPHour(stopwatch)) + " XP/Hr)", statsGuiWidth, startX, y);
                    y += 16;
                }
            }
        }

        if(displayInfoMap != null) {
            for (String string : displayInfoMap.keySet()) {
                SudoGraphics.drawTwoStringsCenterAlignment(g, string, displayInfoMap.get(string), statsGuiWidth, startX, y);
                y += 16;
            }
        }

        SudoGraphics.drawTwoStringsCenterAlignment(g, "Runtime: ", SudoTimer.convertSecondsToFullClockString(stopwatch.getElapsed().getSeconds()), statsGuiWidth, startX, y);

        int taskYBase = 305;
        y = taskYBase;
        g.setColor(new Color(60, 60, 60, 120));
        g.fillRect(4, taskYBase, xAnchor - 4, 32);
        g.setColor(Color.decode("#d5d5d5"));
        g.drawRect(4, taskYBase, xAnchor - 4, 32);
        g.setFont(header2Font);
        y += 12;
        g.setColor(Color.decode("#333333"));
        SudoGraphics.drawHeaderCenterAlignment(g, "Current Task:", xAnchor, 4, y);
        g.setColor(Color.decode(getTitleHexColor()));
        SudoGraphics.drawStringCenterAlignment(g, "Current Task:", xAnchor, 4, y);
        y += 16;

        g.setColor(Color.decode("#d5d5d5"));
        if(currentTask != null)
            SudoGraphics.drawStringCenterAlignment(g, currentTask, xAnchor, 4, y);
    }

    public abstract String getTitle();

    public abstract String getTitleHexColor();

    public abstract BranchTask getRootTask();

    public boolean readyForLogoff() {
        return Game.isLoggedIn();
    }

    public void setCurrentTask(String taskString){
        this.currentTask = taskString;
        Debug.log(taskString);
    }
}