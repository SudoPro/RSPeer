package com.sudo.rspeer.base.ui.awt;

import java.awt.*;

public class SudoGraphics {

    public static void drawTwoStringsCenterAlignment(Graphics g, String stringLeft, String stringRight, int fullWidth, int x, int y) {
        int fontLength = g.getFontMetrics().stringWidth(stringLeft);

        g.drawString(stringLeft, x + fullWidth / 2 - fontLength, y);
        g.drawString(stringRight, x + fullWidth / 2, y);
    }

    public static void drawStringCenterAlignment(Graphics g, String string, int fullWidth, int x, int y) {
        int fontLength = g.getFontMetrics().stringWidth(string);

        g.drawString(string, x + (fullWidth / 2) - (fontLength / 2), y);
    }

    public static void drawHeaderCenterAlignment(Graphics g, String string, int fullWidth, int x, int y) {
        int fontLength = g.getFontMetrics().stringWidth(string);

        g.drawString(string, x + (fullWidth / 2) - (fontLength / 2) - 1, y - 1);
        g.drawString(string, x + (fullWidth / 2) - (fontLength / 2) - 1, y + 1);
        g.drawString(string, x + (fullWidth / 2) - (fontLength / 2) + 1, y - 1);
        g.drawString(string, x + (fullWidth / 2) - (fontLength / 2) + 1, y + 1);
    }

}