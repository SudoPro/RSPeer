package com.sudo.rspeer.base.ui.awt;

public class StringPair {

    private String leftString, rightString;

    public StringPair(String leftString, String rightString){
        this.leftString = leftString;
        this.rightString = rightString;
    }

    public String getLeftString() {
        return leftString;
    }

    public String getRightString() {
        return rightString;
    }
}
