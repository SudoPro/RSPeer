package com.sudo.rspeer.base.ui.fx;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.api.util.Debug;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public abstract class SudoFXController implements Initializable {
    private SudoScript script;

    //<editor-fold desc="BreakHandler Tab">
    @FXML
    public CheckBox BreakHandler_CheckBox;

    @FXML
    public Slider MinBreakDuration_Slider, MaxBreakDuration_Slider, MinDurationBetweenBreak_Slider, MaxDurationBetweenBreak_Slider, StopAfterRuntime_Slider;

    @FXML
    public Text BreakDuration_Text, DurationBetweenBreak_Text, StopAfter_Text;
    //</editor-fold>

    //<editor-fold desc="Settings Tab">
    @FXML
    public CheckBox CameraMiddleMouse_CB, run_CB;
    //</editor-fold>

    public SudoFXController(SudoScript script){
        this.script = script;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        BreakHandler_CheckBox.setOnAction(getBreakHandler_CBEvent());
        
        // Handle Break Duration Minimum value slider
        MinBreakDuration_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() > MaxBreakDuration_Slider.getValue()) {
                MaxBreakDuration_Slider.setValue(newValue.doubleValue());
                script.breakHandler.setMaxBreakDuration(newValue.intValue() * 60000);
            }
            BreakDuration_Text.setText(Integer.toString(newValue.intValue()) + " - " + Integer.toString(script.breakHandler.getMaxBreakDuration() / 60000) + " minutes");
            script.breakHandler.setMinBreakDuration(newValue.intValue() * 60000);
        });

        // Handle Break Duration Maximum value slider
        MaxBreakDuration_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() < MinBreakDuration_Slider.getValue()) {
                MinBreakDuration_Slider.setValue(newValue.doubleValue());
                script.breakHandler.setMinBreakDuration(newValue.intValue() * 60000);
            }
            BreakDuration_Text.setText(Integer.toString(script.breakHandler.getMinBreakDuration() / 60000) + " - " + Integer.toString(newValue.intValue()) + " minutes");
            script.breakHandler.setMaxBreakDuration(newValue.intValue() * 60000);
        });

        // Handle Minimum duration between break value slider
        MinDurationBetweenBreak_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() > MaxDurationBetweenBreak_Slider.getValue()) {
                MaxDurationBetweenBreak_Slider.setValue(newValue.doubleValue());
                script.breakHandler.setMaxDurationBetweenBreak(newValue.intValue() * 60000);
            }
            DurationBetweenBreak_Text.setText(Integer.toString(newValue.intValue()) + " - " + Integer.toString(script.breakHandler.getMaxDurationBetweenBreak() / 60000) + " minutes");
            script.breakHandler.setMinDurationBetweenBreak(newValue.intValue() * 60000);
        });

        // Handle Maximum duration between break value slider
        MaxDurationBetweenBreak_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() < MinDurationBetweenBreak_Slider.getValue()) {
                MinDurationBetweenBreak_Slider.setValue(newValue.doubleValue());
                script.breakHandler.setMinDurationBetweenBreak(newValue.intValue() * 60000);
            }
            DurationBetweenBreak_Text.setText(Integer.toString(script.breakHandler.getMinDurationBetweenBreak() / 60000) + " - " + Integer.toString(newValue.intValue()) + " minutes");
            script.breakHandler.setMaxDurationBetweenBreak(newValue.intValue() * 60000);
        });

        // Handle Stop After Runtime slider
        StopAfterRuntime_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            StopAfter_Text.setText(Integer.toString(newValue.intValue()) + " minutes");
            script.stopAfterMinutes = newValue.intValue();
        });
    }

    public void getStart_BTEvent() {
        script.breakingEnabled = BreakHandler_CheckBox.isSelected();
//        if(script.breakingEnabled) {
//            script.isBreaking = false;
//            script.breakHandler.startTimer();
//            script.setCurrentTask("Next break will occur once Runtime reaches: " + SudoTimer.convertSecondsToFullClockString(script.stopwatch.getElapsed().getSeconds()) + script.breakHandler.getBotTimer().getRemainingTimeInSeconds());
//        }

        script.waitingOnGUI = false;
        Debug.log("GuiWait: " + script.waitingOnGUI);

        script.runEnabled = run_CB.isSelected();
        Debug.log("Run Enabled: " + script.runEnabled);

    }

    // Enable or disable in the BreakHandler tab based on the enabled checkbox
    private EventHandler<ActionEvent> getBreakHandler_CBEvent()
    {
        return event ->
        {
            MinBreakDuration_Slider.setDisable(!BreakHandler_CheckBox.isSelected());
            MaxBreakDuration_Slider.setDisable(!BreakHandler_CheckBox.isSelected());
            MinDurationBetweenBreak_Slider.setDisable(!BreakHandler_CheckBox.isSelected());
            MaxDurationBetweenBreak_Slider.setDisable(!BreakHandler_CheckBox.isSelected());
            BreakDuration_Text.setDisable(!BreakHandler_CheckBox.isSelected());
            DurationBetweenBreak_Text.setDisable(!BreakHandler_CheckBox.isSelected());
        };
    }

}
