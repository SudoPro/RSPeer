package com.sudo.rspeer.base.treebot;

import com.sudo.rspeer.interfaces.treebot.ITreeTask;

public abstract class TreeTask implements ITreeTask {
    @Override
    public abstract void execute();

    @Override
    public abstract TreeTask successTask();

    @Override
    public abstract boolean validate();

    @Override
    public abstract TreeTask failureTask();

    @Override
    public abstract boolean isLeaf();

    public static void Traverse(TreeTask treeTask){
        //Debug.log("TreeTask Classname: " + treeTask.getClass().getName());
        if(!treeTask.isLeaf()) {
            Traverse(treeTask.validate() ? treeTask.successTask() : treeTask.failureTask());
        } else {
            treeTask.execute();
        }
    }
}
