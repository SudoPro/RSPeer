package com.sudo.rspeer.base;

import com.sudo.rspeer.base.treebot.BranchTask;
import com.sudo.rspeer.base.treebot.TreeTask;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.api.util.SudoMath;

import java.text.NumberFormat;
import java.util.Locale;

public abstract class SudoRootTask extends BranchTask {
    private SudoScript script;

    public SudoRootTask (SudoScript script){
        this.script = script;
    }

    @Override
    public TreeTask successTask() {

        if(!script.hasBeenValidated) {
            return script.unauthorizedSessionLeaf;
        } else {
            return script.waitingOnGuiLeaf;
        }
    }

    @Override
    public boolean validate() {
        if (script.hasBeenValidated == null) {
            script.hasBeenValidated = script.authorizedSession();
        }

        validate = script.waitingOnGUI;
        if (script.player != null)
            Debug.log(script.player.getName(), "Root: Waiting On Gui", validate);
        else
            Debug.log("Root: Waiting On Gui: " + validate);
        return validate || !script.hasBeenValidated;
    }

    @Override
    public TreeTask failureTask() {

        // UpdateUI all current values in the XPInfo List
        script.xpInfoMap.values().forEach((xpInfo) -> xpInfo.update());
        updateDisplayMap();
        script.displayInfoMap.put("GP: ", NumberFormat.getNumberInstance(Locale.US).format(script.grossIncome) +
                " (" + NumberFormat.getNumberInstance(Locale.US).format(SudoMath.getItemPerHour(script.stopwatch, script.grossIncome)) + "/Hr)");

        if(script.firstLogin){
            Debug.log("Sessions Settings:");
            Debug.log("Break Enabled: " + script.breakingEnabled);
            if(script.breakingEnabled) {
                Debug.log("Break Duration: " + script.breakHandler.getMinBreakDuration() + " - " + script.breakHandler.getMaxBreakDuration());
                Debug.log("Play Duration: " + script.breakHandler.getMinDurationBetweenBreak() + " - " + script.breakHandler.getMaxDurationBetweenBreak());
            }
            //Debug.log("StopAfterMinutes: " + script.stopAfterMinutes);
        }

        if(script.stopAfterMinutes < script.stopwatch.getElapsed().toMinutes() && script.readyForLogoff()) {
            script.setCurrentTask("User-defined time limit reached...");
            return script.logoutOfGameLeaf;
        }

        if(script.breakingEnabled && script.breakHandler.getBotTimer() == null)
            script.breakHandler.startTimer();

        if(script.breakingEnabled)
            script.breakHandler.execute();

        if(script.breakingEnabled && script.isBreaking && script.readyForLogoff()) {
            Debug.log("Break Triggered.");
            return script.breakHandlerLeaf;
        }

        if(script.isDropping && script.powerDropLeaf != null)
            return script.powerDropLeaf;

        Debug.log("Executing RootTask...");
        return rootTask();
    }

    public abstract TreeTask rootTask();
    public abstract void updateDisplayMap();
}
