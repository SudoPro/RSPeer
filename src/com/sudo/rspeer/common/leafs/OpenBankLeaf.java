package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;
import org.rspeer.runetek.api.component.Bank;

public class OpenBankLeaf extends LeafTask {
    private SudoScript script;

    public OpenBankLeaf (SudoScript script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Opening Bank.");
        Bank.open();
    }
}
