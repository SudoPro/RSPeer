package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.util.Debug;
import org.rspeer.runetek.api.Game;

public class BreakHandlerLeaf extends LeafTask {
    private SudoScript script;

    public BreakHandlerLeaf (SudoScript script){
        this.script = script;
    }

    @Override
    public void execute() {
        if (Game.isLoggedIn()) {
            Debug.log("Attempting to log off.");
            Game.logout();
        }
    }
}
