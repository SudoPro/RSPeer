package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;
import org.rspeer.runetek.api.component.tab.Combat;

public class UseSpecialAttackLeaf extends LeafTask {
    private SudoScript script;

    public UseSpecialAttackLeaf (SudoScript script){
        this.script = script;
    }

    @Override
    public void execute() {
        if (Combat.getSpecialEnergy() >= 100) {
            script.setCurrentTask("Toggling Special Ability.");
            Combat.toggleSpecial(true);
        } else {
            script.setCurrentTask("Special recharging, resetting timer check.");
            script.checkSpecialTimer.reset();
        }
    }
}
