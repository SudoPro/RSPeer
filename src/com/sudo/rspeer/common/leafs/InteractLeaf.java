package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;
import org.rspeer.runetek.adapter.Interactable;

public class InteractLeaf extends LeafTask {
    private SudoScript script;

    private String name, action;
    private Interactable interactable;

    public InteractLeaf (SudoScript script, Interactable interactable, String name, String action){
        this.script = script;
        this.interactable = interactable;
        this.name = name;
        this.action = action;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Interacting with " + name + " using action \"" + action + "\"");
        interactable.interact(action);
    }

    public void setInteractable(Interactable interactable){
        this.interactable = interactable;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAction(String action){
        this.action = action;
    }

    public String getName(){
        return name;
    }

    public String getAction(){
        return action;
    }
}
