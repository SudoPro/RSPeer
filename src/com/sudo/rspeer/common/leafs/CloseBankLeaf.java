package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;
import org.rspeer.runetek.api.component.Bank;

public class CloseBankLeaf extends LeafTask {
    private SudoScript script;

    public CloseBankLeaf(SudoScript script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Closing Bank.");
        Bank.close();
    }
}
