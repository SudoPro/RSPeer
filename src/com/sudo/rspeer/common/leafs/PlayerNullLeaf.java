package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;
import org.rspeer.runetek.api.commons.Time;

public class PlayerNullLeaf extends LeafTask {
    private SudoScript script;

    public PlayerNullLeaf(SudoScript script) {
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Reached PlayerIsNullLeaf...");
        Time.sleep(1000);
    }
}