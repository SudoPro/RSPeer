package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;
import com.sudo.rspeer.api.game.hud.SudoInventory;
import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.interfaces.common.ISuccessAction;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.component.tab.Inventory;

import java.util.Arrays;

public abstract class PowerDropLeaf extends LeafTask implements ISuccessAction {
    private SudoScript script;
    //private String nameContainsString;
    private String[] itemNames = null;
    private boolean validate = false;

    public PowerDropLeaf(SudoScript script) {
        this.script = script;
    }

    public PowerDropLeaf(SudoScript script, String[] itemNames) {
        this.script = script;
        this.itemNames = itemNames;
    }

    public void setValues(String[] itemNames) {
        this.itemNames = itemNames;
    }

    public abstract void successAction();

    @Override
    public void execute() {
        script.isDropping = true;
        Arrays.sort(itemNames);

        if(itemNames != null){
            validate = Inventory.contains(itemNames);
            if(validate) {
                for (Item item : Inventory.getItems()) {
                    if (Arrays.binarySearch(itemNames, item.getName()) >= 0)
                        SudoInventory.dropItem(item);
                }
            } else {
                Debug.log("Item not found in game, dropping successful.");
                script.isDropping = false;
                script.setCurrentTask("Dropping Successful.");
                successAction();
            }
        }

//        validate = Inventory.contains(i -> i.getName().contains(nameContainsString));
//        Debug.log(script.player.getName(), "PowerDrop: InventoryContainsItem", validate);
//        if (validate) {
//            Item[] itemNames = SudoInventory.getItemsWithNameContains(nameContainsString);
//
//            if (itemNames != null) {
//                script.setCurrentTask("Dropping Inventory.");
//                for (Item item : itemNames) {
//                    item.interact("Drop");
//                    Time.sleep(50);
//                }
//            } else {
//                Debug.log("Item not found in game, dropping successful.");
//                script.isDropping = false;
//                script.setCurrentTask("Dropping Successful.");
//                successAction();
//            }
//        } else {
//            Debug.log("Item not found in game, dropping successful.");
//            script.isDropping = false;
//            script.setCurrentTask("Dropping Successful.");
//            successAction();
//        }
    }
}