package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.api.util.Debug;
import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;

public class NotifyLeaf extends LeafTask {
    private SudoScript script;
    private String string = "";

    public NotifyLeaf (SudoScript script){
        this.script = script;
    }

    public NotifyLeaf (SudoScript script, String string){
        this.script = script;
        this.string = string;
    }

    public void setOutputString(String string){
        this.string = string;
    }

    @Override
    public void execute() {
        Debug.log(string);
        script.setCurrentTask(string);
        successAction();
    }

    public void successAction(){};
}
