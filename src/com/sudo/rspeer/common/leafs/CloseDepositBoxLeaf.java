package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;

public class CloseDepositBoxLeaf extends LeafTask {
    private SudoScript script;

    public CloseDepositBoxLeaf(SudoScript script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Closing Deposit Box.");
        //DepositBox.close();
    }
}
