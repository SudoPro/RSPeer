package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;
import org.rspeer.runetek.api.Game;

public class LogoutOfGameLeaf extends LeafTask {
    private SudoScript script;

    public LogoutOfGameLeaf(SudoScript script){
        this.script = script;
    }

    @Override
    public void execute() {
        script.setCurrentTask("Logging out of game.");
        Game.logout();
    }
}
