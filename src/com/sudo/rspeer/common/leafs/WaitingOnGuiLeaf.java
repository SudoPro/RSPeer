package com.sudo.rspeer.common.leafs;

import com.sudo.rspeer.base.SudoScript;
import com.sudo.rspeer.base.treebot.LeafTask;
import org.rspeer.runetek.api.commons.Time;

public class WaitingOnGuiLeaf extends LeafTask {
    private SudoScript script;

    public WaitingOnGuiLeaf (SudoScript script){
        this.script = script;
    }

    @Override
    public void execute(){
        script.setCurrentTask("Waiting on GUI...");
        Time.sleep(500);
    }
}
