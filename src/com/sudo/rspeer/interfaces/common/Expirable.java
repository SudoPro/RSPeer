package com.sudo.rspeer.interfaces.common;

import org.rspeer.runetek.adapter.Interactable;

public interface Expirable<T extends Interactable> {

    boolean hasExpired();

    T getEntity();
}
