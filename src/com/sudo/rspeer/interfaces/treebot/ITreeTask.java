package com.sudo.rspeer.interfaces.treebot;

import com.sudo.rspeer.base.treebot.TreeTask;

public interface ITreeTask {
    void execute();
    TreeTask successTask();
    boolean validate();
    TreeTask failureTask();
    boolean isLeaf();
}
